# Clean-up script which will delete all previous config files and output files before handling new tech-support file 
import os
from logger import *

def clean() :
	if os.path.isfile("outputfiles/Summary.txt"):
		try:
			os.remove("outputfiles/Summary.txt")
			logger_dbg.debug('file Summary deleted')
		except OSError, e:
			logger_dbg.debug("Error : %s - %s " %(e.filename, e.strerror))
	else:
		logger_dbg.debug ("Summary.txt file not found")

	if os.path.isfile("outputfiles/debug_log.log.txt"):
		try:
			os.remove("outputfiles/debug_log.log.txt")
			logger_dbg.debug('file debug_log.log deleted')
		except OSError, e:
			logger_dbg.debug("Error : %s - %s " %(e.filename, e.strerror))
	else:
		logger_dbg.debug ("debug_log.log.txt file not found")

	if os.path.isfile("outputfiles/error_log.log"):
		try:
			os.remove("outputfiles/error_log.log")
			logger_dbg.debug('file error_log.log deleted')
		except OSError, e:
			logger_dbg.debug("Error : %s - %s " %(e.filename, e.strerror))
	else:
		logger_dbg.debug ("error_log.log file not found")

	if os.path.isfile("configfiles/AllObjectGroups.txt"):
		try:
			os.remove("configfiles/AllObjectGroups.txt")
			logger_dbg.debug('file AllObjectGroups deleted')
		except OSError, e:
			logger_dbg.debug("Error : %s - %s " %(e.filename, e.strerror))
	else:
		logger_dbg.debug ("AllObjectGroups.txt file not found")

	if os.path.isfile("configfiles/AllZones.txt"):
		try:
			os.remove("configfiles/AllZones.txt")
			logger_dbg.debug('file AllZones deleted')
		except OSError, e:
			logger_dbg.debug("Error : %s - %s " %(e.filename, e.strerror))
	else:
		logger_dbg.debug ("AllZones.txt file not found")

	if os.path.isfile("configfiles/AllPolicies.txt"):
		try:
			os.remove("configfiles/AllPolicies.txt")
			logger_dbg.debug('file AllPolicies deleted')
		except OSError, e:
			logger_dbg.debug("Error : %s - %s " %(e.filename, e.strerror))
	else:
		logger_dbg.debug ("AllPolicies.txt file not found")

	if os.path.isfile("configfiles/AllRules.txt"):
		try:
			os.remove("configfiles/AllRules.txt")
			logger_dbg.debug('file AllRules deleted')
		except OSError, e:
			logger_dbg.debug("Error : %s - %s " %(e.filename, e.strerror))
	else:
		logger_dbg.debug ("AllRules.txt file not found")

	if os.path.isfile("configfiles/AllSecurityProfiles.txt"):
		try:
			os.remove("configfiles/AllSecurityProfiles.txt")
			logger_dbg.debug('file AllSecurityProfiles deleted')
		except OSError, e:
			logger_dbg.debug("Error : %s - %s " %(e.filename, e.strerror))
	else:
		logger_dbg.debug ("AllSecurityProfiles.txt file not found")

	


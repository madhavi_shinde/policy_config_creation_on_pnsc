#Creating link between policyset and policies
from logger import *
from configfiles import *
import re
import vnmc
import os

class ConfBlk3 :
	tenant = ""
	vdc = ""
	app = ""
	tier = ""
	policy = ""
	policyset = ""
	policies_created = []
	listed = []
	
	def check_tenant_vdc_app_tier(self,name) :
		ConfBlk3.tenant = ""
		ConfBlk3.vdc = ""
		ConfBlk3.app = ""
		ConfBlk3.tier = ""
		name = re.sub("\\n","",name)
		name = re.sub("\\r","",name)
		name = re.sub(".*@","",name)
		entity_list = name.split('/')
		if len(entity_list) == 5 :
			ConfBlk3.tenant = entity_list[1]
			ConfBlk3.vdc = entity_list[2]
			ConfBlk3.app = entity_list[3]
			ConfBlk3.tier = entity_list[4]
		elif len(entity_list) == 4:
			ConfBlk3.tenant = entity_list[1]
			ConfBlk3.vdc = entity_list[2]
			ConfBlk3.app = entity_list[3]
		elif len(entity_list) == 3:
			ConfBlk3.tenant = entity_list[1]
			ConfBlk3.vdc = entity_list[2]
		elif len(entity_list) == 2:
			ConfBlk3.tenant = entity_list[1]
		else :
			ConfBlk3.tenant = entity_list[0]
	
	def policyset_policy_link(self) :		
		with open('configfiles/AllPolicies.txt','r') as policy_data :
			for line in policy_data :
				#temp = ""
				while True :
					try :
						ConfBlk3.listed.append(line)
						line = policy_data.next()
						if 'Policy ' in line :
							temp = line
							break 
					except StopIteration :
						break
		    	# got a policyset block
				#print 'list =' ,ConfBlk3.listed
				ConfBlk3.listed[0] = re.sub("\\n","",ConfBlk3.listed[0])
				ConfBlk3.listed[0] = re.sub("\\r","",ConfBlk3.listed[0])
				self.check_tenant_vdc_app_tier(ConfBlk3.listed[0])
				policy_line =  ConfBlk3.listed[0]
				ConfBlk3.policyset = re.sub("Policy ","",policy_line)
				#ConfBlk3.policyset = re.sub("policy ","",policy_line)
				ConfBlk3.policyset = re.sub("@root.*","",ConfBlk3.policyset)
				ConfBlk3.listed.pop(0)
				for rule_line in ConfBlk3.listed :
					ConfBlk3.policy = re.sub("rule ","",rule_line)
					ConfBlk3.policy = re.sub("/.*","",ConfBlk3.policy)
					order = re.sub(".*order ","",rule_line)
					ConfBlk3.policies_created.append(ConfBlk3.policy + " " + order)
				ConfBlk3.policies_created = list(set(ConfBlk3.policies_created))
				if not ConfBlk3.policies_created: 
					# check if ConfBlk3.policy is empty
		    	    #No policies available to link to policyset
					pass
				else: 
					#calling function which will create XML file linking 
#those policies to ConfBlk3.ConfBlk3.policyset
					response = vnmc.policyset_modify(ConfBlk3.vdc, ConfBlk3.tenant, ConfBlk3.app, ConfBlk3.tier, ConfBlk3.policyset , "", ConfBlk3.policies_created)
					if "skip" not in response : 
						print 'Attached policyset ',ConfBlk3.policyset,'to respected policies'
					else :
						print 'Already attached policyset ',ConfBlk3.policyset,'to respected policies'
				ConfBlk3.listed = []
				ConfBlk3.policies_created = []
				ConfBlk3.listed.append(temp)

#def main () :
    #ConfBlk3.ConfBlk3.policyset_ConfBlk3.policy_link()

#if __name__ == '__main__' :
    #main()


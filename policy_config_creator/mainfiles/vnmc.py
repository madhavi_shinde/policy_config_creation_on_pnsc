import StringIO
from logger import *
#import logger_dbg
from vsg.vnmc_xml import *
from vsg.variables import *
from vsg.parse_xml import *

'''
This method displays count of how many configs push successfully and how many fails
'''
def display_count_of_configs():
    with open("outputfiles/Summary.txt","w") as summary_data :
        summary_data.write('\nTotal number of tenants is ' + str(variables.tenant_count_total))
        print '\nTotal number of tenants is',variables.tenant_count_total
        summary_data.write('\nOut of this,' + str(variables.tenant_count_pass) + ' tenants pass')
        summary_data.write('\nOut of this,' + str(variables.tenant_count_fail) + ' tenants fails')

        summary_data.write('\nTotal number of VDCs is '+str(variables.vdc_count_total))
        print '\nTotal number of VDCs is',variables.vdc_count_total
        summary_data.write('\nOut of this,' + str(variables.vdc_count_pass) + ' VDCs pass')
        summary_data.write('\nOut of this,' + str(variables.vdc_count_fail) + ' VDCs fails')

        summary_data.write('\nTotal number of applications is '+str(variables.app_count_total))
        print '\nTotal number of applications is',variables.app_count_total
        summary_data.write('\nOut of this,'+str(variables.app_count_pass)+' applications pass')
        summary_data.write('\nOut of this,'+str(variables.app_count_fail)+' applications fails')

        summary_data.write('\nTotal number of tiers is '+str(variables.tier_count_total))
        print '\nTotal number of tiers is',variables.tier_count_total
        summary_data.write('\nOut of this,'+str(variables.tier_count_pass)+' tiers pass')
        summary_data.write('\nOut of this,'+str(variables.tier_count_fail)+' tiers fails')

        summary_data.write('\nTotal number of security-profiles is '+ str(variables.sec_pro_count_total))
        print '\nTotal number of security-profiles is',variables.sec_pro_count_total
        summary_data.write('\nOut of this,'+ str(variables.sec_pro_count_pass)+' security-profiles pass')
        summary_data.write('\nOut of this,'+ str(variables.sec_pro_count_fail)+' security-profiles fails')

        summary_data.write('\nTotal number of policysets is '+ str(variables.policyset_count_total))
        print '\nTotal number of policysets is',variables.policyset_count_total
        summary_data.write('\nOut of this,'+ str(variables.policyset_count_pass)+' policysets pass')
        summary_data.write('\nOut of this,'+ str(variables.policyset_count_fail)+' policysets fails')

        summary_data.write('\nTotal number of policies is '+str(variables.policy_count_total))
        print '\nTotal number of policies is',variables.policy_count_total
        summary_data.write('\nOut of this,'+str(variables.policy_count_pass)+' policies pass')
        summary_data.write('\nOut of this,'+str(variables.policy_count_fail)+' policies fails')

        summary_data.write('\nTotal number of rules is '+ str(variables.rule_count_total))
        print '\nTotal number of rules is',variables.rule_count_total
        summary_data.write('\nOut of this,'+ str(variables.rule_count_pass) +' rules pass')
        summary_data.write('\nOut of this,'+ str(variables.rule_count_fail) +' rules fails')

        summary_data.write('\nTotal number of object-groups is '+ str(variables.objgrp_count_total))
        print '\nTotal number of object-groups is',variables.objgrp_count_total
        summary_data.write('\nOut of this,'+ str(variables.objgrp_count_pass) +' object-groups pass')
        summary_data.write('\nOut of this,'+ str(variables.objgrp_count_fail) +' object-groups fails')

        summary_data.write('\nTotal number of zones is '+ str(variables.zone_count_total))
        print '\nTotal number of zones is',variables.zone_count_total
        summary_data.write('\nOut of this,'+ str(variables.zone_count_pass) +' zones pass')
        summary_data.write('\nOut of this,'+ str(variables.zone_count_fail) +' zones fails')

'''
This method checks whether xml fails or pass
'''
def check_error(response):
    response = response.text
    response = response.rstrip().split('\n')
    err_code = ""
    err_desc = ""
    for resp in response:
            temp = resp.split('=')
            if temp[0] == " errorCode":
            	err_code = temp[1]
	    if temp[0] == " errorDescr":
		err_desc = temp[1]
		break
    return err_code, err_desc

'''
This method checks whether xml fails or pass by checking status value
'''
def check_status(response,mode):
    response = response.text
    response = response.rstrip().split('\n')
    status_value = ""
    for resp in response:
            temp = resp.split('=')
            #print temp
            if temp[0] == "  status":
                status_value = temp[1]
                break
    return status_value
    
'''
This method creates New Application
'''
def application_create(tenant, vdc, app, app_desc):
    cookie = get_cookie(vnmc_ip,vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header_zone(output, cookie)
    print_application("yes", output, tenant, vdc, app, app_desc)
    print_set_footer_zone(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "service-reg")
    err_code,err_desc = check_error(response)
    variables.app_count_total += 1
    if err_code :
	    variables.app_count_fail += 1
	    logger_err.error('\nError occure while pushing xml data for application %s' % 
app)
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
	    variables.app_count_pass += 1
    return response

'''
This method creates New Tier
'''
def tier_create(tenant, vdc, app, tier, tier_desc):
    cookie = get_cookie(vnmc_ip,vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header_zone(output, cookie)
    print_tier("yes", output, tenant, vdc, app, tier, tier_desc)
    print_set_footer_zone(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "service-reg")
    err_code,err_desc = check_error(response)
    variables.tier_count_total += 1
    if err_code :
	    variables.tier_count_fail += 1
	    logger_err.error('\nError occure while pushing xml data for tier %s' % 
tier)
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
	    variables.tier_count_pass += 1
    return response


'''
This method creates New Virtual Data Center
'''
def vdc_create(tenant, vdc_name, vdc_desc):
    cookie = get_cookie(vnmc_ip,vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header_zone(output, cookie)
    print_vdc("yes", output, tenant, vdc_name, vdc_desc)
    print_set_footer_zone(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "service-reg")
    err_code,err_desc = check_error(response)
    variables.vdc_count_total += 1
    if err_code :
	    variables.tier_count_fail += 1
	    logger_err.error('\nError occure while pushing xml data for vdc %s' % 
vdc_name)
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
	    variables.tier_count_pass += 1
    return response

'''
This method creates New Rule and attached it to policy
'''
def policy_modify(vdc, tenant, app, tier, policyname, rulename, main_order, desc, action_type, conditions,match_cri):
    cookie = get_cookie(vnmc_ip,vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_rule("yes", output, vdc, tenant, app, tier, policyname, rulename, 
main_order, desc, action_type, conditions,match_cri)
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    err_code,err_desc = check_error(response)
    variables.rule_count_total += 1
    if err_code :
	    variables.rule_count_fail += 1
	    logger_err.error('\nError occure while pushing xml data for rule %s' % 
rulename)
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
	    variables.rule_count_pass += 1
    return response

'''
This method creates New Policy
'''
def policy_create(vdc, tenant, app, tier, policyname, desc):
    cookie = get_cookie(vnmc_ip,vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header_zone(output, cookie)
    print_policy("yes", output, vdc, tenant, app, tier, policyname, desc)
    print_set_footer_zone(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    status_value = check_status(response,"created")
    variables.policy_count_total += 1
    if not status_value :
        variables.policy_count_fail += 1
        logger_err.error('\nError occure while pushing xml data for policy %s' % 
policyname)
        return "skip"
    else :
        variables.policy_count_pass += 1
        return response

'''
This method creates New Tenant
'''
def tenant_create(tenantname, desc):
    cookie = get_cookie(vnmc_ip,vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_tenant("yes", output, cookie, tenantname, desc)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "service-reg")
    err_code,err_desc = check_error(response)
    variables.tenant_count_total += 1
    if err_code :
	variables.tenant_count_fail += 1
 	print '\nError occure while pushing xml data for tenant ',tenantname
    	print 'Error code = ',err_code,' and error desciption = ',err_desc
	logger_err.error('\nError occure while pushing xml data for tenant %s' % 
tenantname)
	logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
   	ch = raw_input("Do you want to continue with next :")
 	lists = ['yes','y','Y','YES'] 
        if any(x in ch for x in lists) : 
	    return "skip"
    else :
        variables.tenant_count_pass += 1
        return response

'''
This method deletes Tenant
'''
def tenant_delete(name):
    cookie = get_cookie(vnmc_ip,vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_tenant("no", output, cookie, name, "")
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "service-reg")
    status_value = check_status(response,"deleted")
    if not status_value :
        logger_err.error('\nError occure while pushing xml data for deleting tenant %s' % 
name)
        logger_err.error('Status value = %s ' % status_value)
        return "Error"
    else :
        return response

'''   
This method attaches Policies to policyset
'''
def policyset_modify(vdc, tenant, app, tier, psname , ps_desc, policies_list):
    logger_dbg.debug("In vnmc.py Descr:" + ps_desc)
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_policy_set_with_policies("yes", output, vdc, tenant, app, tier, psname, ps_desc, 
policies_list)
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    err_code,err_desc = check_error(response)
    if err_code :
	    logger_err.error('\nError occure while pushing xml data for policyset %s' % 
psname)
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
        return response


'''   
This method creates New PolicySet
'''
def policyset_create(vdc, tenant, app, tier, psname , ps_desc):
    logger_dbg.debug("In vnmc.py Descr:" + ps_desc)
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header_zone(output, cookie)
    print_policy_set("yes", output, vdc, tenant, app, tier, psname, ps_desc)
    print_set_footer_zone(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    err_code,err_desc = check_error(response)
    variables.policyset_count_total += 1
    if err_code :
	    variables.policyset_count_fail += 1
 	    logger_err.error('\nError occure while pushing xml data for policyset %s' % 
psname)
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
	    variables.policyset_count_pass += 1
    return response

'''
This method creates New Security Profile
'''
def security_profile_create(vdc, tenant, spname, desc, psname):
    logger_dbg.debug("In vnmc.py Descr:" + desc)
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_security_profile("yes", output, vdc, tenant, spname, desc, psname)
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    err_code,err_desc = check_error(response)
    variables.sec_pro_count_total += 1
    if err_code :
	    variables.sec_pro_count_pass += 1
	    logger_err.error('\nError occure while pushing xml data for security \
profile %s' % spname)
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
	    variables.sec_pro_count_fail += 1
    return response

'''
This method modifies Security Profile
'''

def security_profile_modify(vdc, tenant, app, tier, spname, desc, psname, custm_attr_list):
    logger_dbg.debug("In vnmc.py Descr:" + desc)
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_security_profile_modify("yes", output, vdc, tenant, app, tier, spname, desc, psname, custm_attr_list)
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    err_code,err_desc = check_error(response)
    variables.sec_pro_count_total += 1
    if err_code :
	    variables.sec_pro_count_fail += 1
	    logger_err.error('\nError occure while pushing xml data for security \
profile %s' % spname)
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
	    variables.sec_pro_count_pass += 1
    return response

'''
This method create empty dictionary for custom attributes of Security Profile
'''

def dictionary_create(tenant, dictname):
    logger_dbg.debug("In vnmc.py Descr: dictionary")
    print 'vnmc_ip' , vnmc_ip
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header_zone(output, cookie)
    print_security_profile_dictionary_empty(output, tenant, dictname)
    print_set_footer_zone(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    err_code,err_desc = check_error(response)
    if err_code :
	    if tenant == "" :
	        tenant = "root"
	    logger_err.error('\nError occure while pushing xml data for dictionary \
%s of tenant %s ' % (dictname,tenant))
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"
    else :
        return response

'''
This method modify dictionary for custom attributes of Security Profile
'''

def dictionary_modify(tenant, dictname, custm_attr_list):
    logger_dbg.debug("In vnmc.py Descr: dictionary")
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_security_profile_dictionary_modify(output, tenant, dictname, 
custm_attr_list)
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    err_code,err_desc = check_error(response)
    if err_code :
	    logger_err.error('\nError occure while pushing xml data for modifying \
dictionary %s of tenant %s ' % (dictname,tenant))
	    logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
	    return "skip"	
    else :
	    return response

'''
This method deletes the Security Profile
'''

def security_profile_delete(tenant, name):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    desc = ""
    print_security_profile("no", output, tenant, name, desc, name + "-PS")
    print_policy_set("no", output, tenant, name + "-PS", name + "-PS-P")
    print_policy("no", output, tenant, name + "-PS-P")
    print_set_footer(output)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

'''
'''
def get_security_profile_table(tenant_name):
    security_profiles = []
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_sp_get_request(output, cookie, tenant_name)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    security_profiles = parse_sptable_xml2json(response.content)
    return security_profiles

def get_security_profile_rules(tenant, sp_name):
    rules = []
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_rules_get_request(output, cookie, tenant, sp_name)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    rules = parse_rule_table_xml2json(response.content)
    return rules

def get_rule_conditions(tenant, sp_name, rule_name):
    conds = []
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_conds_get_request(output, cookie, tenant, sp_name, rule_name)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    conds = parse_cond_table_xml2json(response.content)
    return conds

'''
'''
def security_profile_rule_create(tenant, sp_name, rule_name, order, description, 
action, conditions):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_rule("yes", output, tenant, sp_name + "-PS-P", rule_name, order, 
description, action, conditions)
    print_set_footer(output)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response


'''
'''
def security_profile_rule_delete(tenant, sp_name, rule_name):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_rule("no", output, tenant, sp_name + "-PS-P", rule_name, "", "", "", [])
    print_set_footer(output)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

'''
'''
def rule_condition_create(tenant, sp_name, rule_name, condition_number, attribute,
 operator, value):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_rule_condition("yes", output, tenant, sp_name, rule_name, condition_number, attribute, operator, value)
    print_set_footer(output)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response


'''
'''
def rule_condition_delete(tenant, sp_name, rule_name, condition_number):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_rule_condition("no", output, tenant, sp_name, rule_name, condition_number,
 "", "", "")
    print_set_footer(output)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

'''
This method creates object-group
'''

def object_group_create(vdc, tenant, app, tier, ogroupname, description, attr_name, conditions):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_objgrp("yes", output, vdc, tenant, app, tier, ogroupname, description, attr_name, 
conditions)
    #print_objgrp_condition("yes", output, tenant, ogroupname, attr_name, conditions)
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    status_value = check_status(response,"created")
    variables.objgrp_count_total += 1
    if not status_value :
        variables.objgrp_count_fail += 1
        logger_err.error('\nError occure while pushing xml data for object-group \
%s' % ogroupname)
        return "skip"
    else :
        variables.objgrp_count_pass += 1
        return response
'''
'''
def object_group_delete(tenant, ogroupname):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_objgrp("no", output, tenant, ogroupname, "")
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

'''
'''
def object_group_condition_create(tenant, ogroupname, condition_number, attr_name, operator_type, value):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_objgrp_condition("yes", output, tenant, ogroupname, condition_number, 
attr_name, operator_type, value)
    print_set_footer(output)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

'''
'''
def object_group_condition_delete(tenant, ogroupname, condition_number):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_objgrp_condition("no", output, tenant, ogroupname, condition_number,
 "", "", "")
    print_set_footer(output)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

def get_ogroups(tenant):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_ogroup_get_request(output, cookie, tenant)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    ogroups = parse_ogrouptable_xml2json(response.content)
    return ogroups

def get_ogroup_conditions(tenant, ogroup_name):
    conds = []
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_ogroup_conds_get_request(output, cookie, tenant, ogroup_name)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    conds = parse_ogroup_cond_table_xml2json(response.content)
    return conds

def get_zones(tenant):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_zone_get_request(output, cookie, tenant)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    zones = parse_zonetable_xml2json(response.content)
    return zones

def zone_create(vdc, tenant, app, tier, zonename, desc, match_cond, conditions):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_zone("yes", output, vdc, tenant, app, tier, zonename, desc, match_cond, conditions)
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    err_code,err_desc = check_error(response)
    variables.zone_count_total += 1
    if err_code :
        variables.zone_count_fail += 1
        logger_err.error('\nError occure while pushing xml data for zone \
%s' % zonename)
        logger_err.error('Error code = %s and error desciption = %s' % 
(err_code, err_desc))
        return "skip"
    else :
        variables.zone_count_pass += 1
        return response

def get_zone_conditions(tenant, zone_name):
    conds = []
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    logger_dbg.debug("============= get_zone_conditions =================")
    print_zone_conds_get_request(output, cookie, tenant, zone_name)
    logger_dbg.debug("=============print_zone_conds_get_request==========")
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    conds = parse_zone_cond_table_xml2json(response.content)
    return conds

def zone_condition_create(tenant, zonename, condition_number, attr_name, 
operator_type, value):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_zone_condition("yes", output, tenant, zonename, condition_number, attr_name, operator_type, value)
    print_set_footer(output)
    logger_dbg.debug("====================print_output_value=========================")
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

def zone_condition_delete(tenant, zonename, condition_number):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_zone_condition("no", output, tenant, zonename, condition_number, "", 
"", "")
    print_set_footer(output)
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

def zone_delete(tenant, zonename):
    cookie = get_cookie(vnmc_ip, vnmc_user, vnmc_pass)
    output = StringIO.StringIO()
    print_set_header(output, cookie)
    print_zone("no", output, tenant, zonename, "")
    print_set_footer(output)
    logger_dbg.debug(output.getvalue())
    response = push_config(vnmc_ip, output.getvalue(), "policy-mgr")
    output.close()
    return response

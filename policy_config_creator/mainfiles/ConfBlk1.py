# This program creates config-block1 which includes ConfBlk1.tenant,SP and PS
from logger import *
from configfiles import *
import re
import os
import vnmc

class ConfBlk1 :

	tenant = ""
	vdc = ""
	app = ""
	tier = ""
	listed = []
	tenants_created = []
	policysets_created = []
	vdcs_created = []
	apps_created = []
	tiers_created = []
	dictname = "default_tenant"
	root_dict_flag = False

	def custom_attr_add_to_dict(self,custm_attr_list, custm_attr_list_added) :
		#this function add custm_attr_list to dictionary
		attr_to_be_added = []
		current_tenant = ""
		if not custm_attr_list_added :
			for new_custm_attr in custm_attr_list :
				new_value = new_custm_attr.split()
				current_tenant = new_value[2]
				attr_to_be_added.append(new_value[0])
				custm_attr_list_added.append(new_value[0] + ' ' + new_value[2])
		else :
			for new_custm_attr in custm_attr_list :
				find_flag = True
				for old_custm_attr in custm_attr_list_added :
					new_value = new_custm_attr.split()
					current_tenant = new_value[2]
					new_attr = new_value[0] + ' ' + new_value[2]
					old_value = old_custm_attr.split()
					old_attr = old_value[0] + ' ' + old_value[1]
					if new_attr != old_attr :
						find_flag = False
					else :
						find_flag = True
						break
				if find_flag == False :
					attr_to_be_added.append(new_value[0])
					custm_attr_list_added.append(new_value[0] + ' ' + new_value[2])
		if current_tenant != "" : #if there is a attribute in custm_attr_list
			if current_tenant == 'root' :
				current_tenant = ""
			response = vnmc.dictionary_modify(current_tenant, ConfBlk1.dictname, attr_to_be_added)
			if "skip" not in response : 
				print 'Dictionary ',ConfBlk1.dictname,' modified'
			else :
				print 'Dictionary ',ConfBlk1.dictname,'already created ,skipping it'
		return custm_attr_list_added
    
	def custom_attr_create(self) :
		custm_attr_list = []
		for line in ConfBlk1.listed :
			if "security-profile" in line :
				def_vnsporg_val = re.sub(".*@","",line)
				def_vnsporg_val = re.sub("\\n","",def_vnsporg_val)
				def_vnsporg_val = re.sub("\\r","",def_vnsporg_val)
				def_vnsporg_val.replace(" ","")
				def_vnsporg_val = def_vnsporg_val.lower()
			if "custom-attribute" in line :
				values = line.split()
				attr_name = values[1]
				value = values[2]
				value = re.sub("\"","",value)
				if attr_name == "vnsporg" :
					if value == def_vnsporg_val :
						pass
					else : 
						custm_attr_list.append(attr_name + ' ' + value + ' ' + ConfBlk1.tenant)
				else :
					custm_attr_list.append(attr_name + ' ' + value + ' ' + ConfBlk1.tenant)
		return custm_attr_list

	def create_entities(self) :
		ConfBlk1.tenant = ""
		ConfBlk1.vdc = ""
		ConfBlk1.app = ""
		ConfBlk1.tier = ""
		name = ConfBlk1.listed[0]
		name = re.sub(".*@","",name)
		entity_list = name.split('/')
		if len(entity_list) == 5 :
			ConfBlk1.tenant = entity_list[1]
			ConfBlk1.vdc = entity_list[2]
			ConfBlk1.app = entity_list[3]
			ConfBlk1.tier = entity_list[4]
		elif len(entity_list) == 4:
			ConfBlk1.tenant = entity_list[1]
			ConfBlk1.vdc = entity_list[2]
			ConfBlk1.app = entity_list[3]
		elif len(entity_list) == 3:
			ConfBlk1.tenant = entity_list[1]
			ConfBlk1.vdc = entity_list[2]
		elif len(entity_list) == 2:
			ConfBlk1.tenant = entity_list[1]
		else :
			ConfBlk1.tenant = entity_list[0]


	def tenant_sp_ps_link(self) :
		custm_attr_list_added = []
		custm_attr_list = []
		with open("configfiles/AllSecurityProfiles.txt","r") as sp_data :
			for line in sp_data :
				while True :
					if line in ('\n' , '\r\n') :
						break
					ConfBlk1.listed.append(line)
					try :
						line = sp_data.next()
					except StopIteration :
						break
				# got a SP block
				ConfBlk1.listed[0] = re.sub("\\n","",ConfBlk1.listed[0])
				ConfBlk1.listed[0] = re.sub("\\r","",ConfBlk1.listed[0])
				self.create_entities()
				sp_line =  ConfBlk1.listed[0]
				sp = re.sub("security-profile ","",sp_line)
				sp = re.sub("@root.*","",sp)
				sp.replace(" ","")
				sp_line = ConfBlk1.listed[0]
				policyset_line = ConfBlk1.listed[1]
				policyset_line = re.sub("\\n","",policyset_line)
				policyset_line = re.sub("\\r","",policyset_line)
				policyset = re.sub(".*policy ","",policyset_line)
				policyset_with_tenant = policyset
				policyset = re.sub("@root.*","",policyset)
				custm_attr_list = self.custom_attr_create()
				if not ConfBlk1.tenants_created :
  	                #list is empty
					if ConfBlk1.tenant != "root" :
						ConfBlk1.tenant_desc = "tenant"
						response = vnmc.tenant_create(ConfBlk1.tenant, ConfBlk1.tenant_desc)
						if "skip" not in response : 
							print 'tenant ',ConfBlk1.tenant,' created'
						else :
							#it means
							print 'tenant ',ConfBlk1.tenant,'already created ,skipping it'
						if ConfBlk1.vdc != "":
							response = vnmc.vdc_create(ConfBlk1.tenant, ConfBlk1.vdc, "")
							ConfBlk1.vdcs_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc)
							if "skip" not in response : 
								print 'vdc ',ConfBlk1.vdc,' created'
							else :
								print 'vdc ',ConfBlk1.vdc,'already created ,skipping it'
							if ConfBlk1.app != "":
								response = vnmc.application_create(ConfBlk1.tenant, ConfBlk1.vdc, ConfBlk1.app, "")
								ConfBlk1.apps_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc + '\\' + ConfBlk1.app)
								if "skip" not in response : 
									print 'application ',ConfBlk1.app,' created'
								else :
									print 'application ',ConfBlk1.app,'already created ,skipping it'
								if ConfBlk1.tier != "" :
									response = vnmc.tier_create(ConfBlk1.tenant, ConfBlk1.vdc, ConfBlk1.app, ConfBlk1.tier, "")
									ConfBlk1.tiers_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc + '\\' + ConfBlk1.app + '\\' + ConfBlk1.tier)
									if "skip" not in response : 
										print 'tier ',ConfBlk1.tier,' created'
									else :
										print 'tier ',ConfBlk1.tier,'already created ,skipping it'
						response = vnmc.dictionary_create(ConfBlk1.tenant, ConfBlk1.dictname)
						if "skip" not in response : 
							print 'created dict for tenant ', ConfBlk1.tenant
						else :
							print 'Already created dict for tenant ', ConfBlk1.tenant
					else :
						response = vnmc.dictionary_create("", ConfBlk1.dictname)
						if "skip" not in response : 
							print 'created dict for tenant root'
						else :
							print 'Already created dict for tenant root'
						ConfBlk1.root_dict_flag = True
					ConfBlk1.tenants_created.append(ConfBlk1.tenant)
				if not ConfBlk1.policysets_created :
		            #list is empty
					if policyset != "default" :
						ps_desc = "Policy Set "
						if ConfBlk1.tenant == "root" :   
							response = vnmc.policyset_create("", "", "", "", policyset, ps_desc)
							if "skip" not in response : 
								print 'policyset ',policyset,' created'
							else :
								print 'policyset ',policyset,'already created ,skipping it'
						else :
							response = vnmc.policyset_create(ConfBlk1.vdc, ConfBlk1.tenant, ConfBlk1.app, ConfBlk1.tier, policyset, ps_desc)
							if "skip" not in response : 
								print 'policyset ',policyset,' created'
							else :
								print 'policyset ',policyset,'already created ,skipping it'
					else :
						pass
						logger_dbg.debug('policyset default is already there')
					ConfBlk1.policysets_created.append(policyset_with_tenant)
				if ConfBlk1.tenant in ConfBlk1.tenants_created :
					print 'tenant is already created ... '+ ConfBlk1.tenant
					if ConfBlk1.vdc != "" :
						ConfBlk1.vdc_with_tenant = ConfBlk1.tenant + '\\' + ConfBlk1.vdc
						if ConfBlk1.vdc_with_tenant in ConfBlk1.vdcs_created :
							logger_dbg.debug('vdc is already there for %s tenant' % ConfBlk1.tenant)
						else :
							response = vnmc.vdc_create(ConfBlk1.tenant, ConfBlk1.vdc, "")
							ConfBlk1.vdcs_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc)
							if "skip" not in response : 
								print 'vdc ',ConfBlk1.vdc,' created'
							else :
								print 'vdc ',ConfBlk1.vdc,'already created ,skipping it'

						if ConfBlk1.app != "" :
							ConfBlk1.app_with_path = ConfBlk1.tenant + '\\' + ConfBlk1.vdc + '\\' + ConfBlk1.app
							if ConfBlk1.app_with_path in ConfBlk1.apps_created :
								logger_dbg.debug('app is already there for %s tenant and %s ConfBlk1.vdc' % (ConfBlk1.tenant, ConfBlk1.vdc))
							else :
								response = vnmc.application_create(ConfBlk1.tenant, ConfBlk1.vdc, ConfBlk1.app, "application")
								ConfBlk1.apps_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc + '\\' + ConfBlk1.app)
								if "skip" not in response : 
									print 'app ',ConfBlk1.app,' created'
								else :
									print 'app ',ConfBlk1.app,'already created ,skipping it'

							if ConfBlk1.tier != "" :
								ConfBlk1.tier_with_path = ConfBlk1.tenant + '\\' + ConfBlk1.vdc + '\\' + ConfBlk1.app + '\\' + ConfBlk1.tier
								if ConfBlk1.tier_with_path in ConfBlk1.tiers_created :
									logger_dbg.debug('tier is already there for %s tenant and %s vdc and under %s application ' % (ConfBlk1.tenant, ConfBlk1.vdc, ConfBlk1.app))
								else :
									response = vnmc.tier_create(ConfBlk1.tenant, ConfBlk1.vdc, ConfBlk1.app, ConfBlk1.tier,  "application")
									ConfBlk1.tiers_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc + '\\' + ConfBlk1.app + '\\' + ConfBlk1.tier)
									if "skip" not in response : 
										print 'tier ',ConfBlk1.tier,' created'
									else :
										print 'tier ',ConfBlk1.tier,'already created ,skipping it'

					if policyset_with_tenant in ConfBlk1.policysets_created :
						logger_dbg.debug('policyset is already created.')
		            #elif policyset + '@root' in ConfBlk1.policysets_created and 'root/' not in policyset_with_ConfBlk1.tenant:
		                #logger_dbg.debug('policyset is already created at root ConfBlk1.tenant')
					else :
						if policyset != "default":
							ps_desc = "Policy Set "
							if ConfBlk1.tenant == "root" :  
								response = vnmc.policyset_create("", "", "", "", policyset,
ps_desc)
								if "skip" not in response :
									print 'policyset ',policyset,' created'
								else :
									print 'policyset ',policyset,'already created ,skipping it'
							else :
								response = vnmc.policyset_create(ConfBlk1.vdc, ConfBlk1.tenant, ConfBlk1.app, ConfBlk1.tier, policyset, ps_desc)
								if "skip" not in response : 
									print 'policyset ',policyset,' created'
								else :
									print 'policyset ',policyset,'already created ,skipping it'
							ConfBlk1.policysets_created.append(policyset_with_tenant)
					if ConfBlk1.tenant != "root" and sp != "default" :	        
						# Attaching PS to SP
						sp_desc = "security-profile "
						custm_attr_list_added = self.custom_attr_add_to_dict(custm_attr_list, custm_attr_list_added)
						response = vnmc.security_profile_modify(ConfBlk1.vdc, ConfBlk1.tenant, ConfBlk1.app, ConfBlk1.tier, sp, sp_desc, policyset, custm_attr_list)
						if "skip" not in response : 
							print 'Created SP', sp ,' and attached PS ' ,policyset, ' to SP'
						else :
							print 'Already created SP', sp ,' and attached PS ' ,policyset, ' to SP'
					else : 
						# we are attaching sum PS to default SP of root ConfBlk1.tenant
						if policyset != "default" :
							sp_desc = "security-profile "
							custm_attr_list_added = self.custom_attr_add_to_dict(custm_attr_list, custm_attr_list_added)
							response = vnmc.security_profile_modify("", "", "", "", sp, sp_desc, policyset, custm_attr_list)
							if "skip" not in response : 
								print 'Created SP', sp ,' and attached PS ' ,policyset, ' to SP'
							else :
								print 'Already created SP', sp ,' and attached PS ' ,policyset, ' to SP'  
						else :
							logger_dbg.debug('Security profile is already created.')
				if ConfBlk1.tenant not in ConfBlk1.tenants_created :
					if ConfBlk1.tenant != "root" :
						ConfBlk1.tenant_desc = "Tenant "
						response = vnmc.tenant_create(ConfBlk1.tenant, ConfBlk1.tenant_desc)
						if ConfBlk1.vdc != "":
							response = vnmc.vdc_create(ConfBlk1.tenant, ConfBlk1.vdc, "")
							ConfBlk1.vdcs_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc)
							if "skip" not in response : 
								print 'vdc ',ConfBlk1.vdc,' created'
							else :
								print 'vdc ',ConfBlk1.vdc,'already created ,skipping it'
			
							if ConfBlk1.app != "":
								response = vnmc.application_create(ConfBlk1.tenant, ConfBlk1.vdc, ConfBlk1.app, "")
								ConfBlk1.apps_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc + '\\' + ConfBlk1.app)
								if "skip" not in response : 
									print 'application ',ConfBlk1.app,' created'
								else :
									print 'application ',ConfBlk1.app,'already created ,skipping it'
								if ConfBlk1.tier != "":
									response = vnmc.tier_create(ConfBlk1.tenant, ConfBlk1.vdc, ConfBlk1.app, ConfBlk1.tier, "")
									ConfBlk1.tiers_created.append(ConfBlk1.tenant + '\\' + ConfBlk1.vdc + '\\' + ConfBlk1.app + '\\' + ConfBlk1.tier)
									if "skip" not in response : 
										print 'tier ',ConfBlk1.tier,' created'
									else :
										print 'tier ',ConfBlk1.tier,'already created ,skipping it'
						if "skip" not in response : 
							print 'tenant ',ConfBlk1.tenant,' created'
						else :
							print 'tenant ',ConfBlk1.tenant,'already created ,skipping it'
						response = vnmc.dictionary_create(ConfBlk1.tenant, ConfBlk1.dictname)
						if "skip" not in response : 
							print 'created dict for tenant ', ConfBlk1.tenant
						else :
							print 'already created dict for tenant ', ConfBlk1.tenant
					else :
						if ConfBlk1.root_dict_flag == False :
							if "skip" not in response : 
								print 'created dict for tenant root'
							else :
								print 'Already created dict for tenant root'
							ConfBlk1.root_dict_flag = True
						logger_dbg.debug('tenant root is already there....')
					ConfBlk1.tenants_created.append(ConfBlk1.tenant)
					if policyset_with_tenant in ConfBlk1.policysets_created :
						logger_dbg.debug( 'policyset is already created.')
					else :
						if policyset != "default":
							ps_desc = "Policy Set "
							if ConfBlk1.tenant == "root" :
								response = vnmc.policyset_create("", "", "", "", policyset,
ps_desc)
								if "skip" not in response : 
									print 'policyset ',policyset,' created'
								else :
									print 'policyset ',policyset,'already created ,skipping it'
							else :
								response = vnmc.policyset_create(ConfBlk1.vdc, ConfBlk1.tenant, ConfBlk1.app, ConfBlk1.tier, policyset,ps_desc)
								if "skip" not in response : 
									print 'policyset ',policyset,' created'
								else :
									print 'policyset ',policyset,'already created ,skipping it'
						else :
							pass
							logger_dbg.debug('policy set default is already there ')         
						ConfBlk1.policysets_created.append(policyset_with_tenant)
					if ConfBlk1.tenant != "root" and sp != "default" :
						sp_desc = "security-profile "
						custm_attr_list_added = self.custom_attr_add_to_dict(custm_attr_list, custm_attr_list_added)
						response = vnmc.security_profile_modify(ConfBlk1.vdc, ConfBlk1.tenant, ConfBlk1.app, ConfBlk1.tier, sp, sp_desc, policyset, custm_attr_list)
						if "skip" not in response : 
							print 'created SP', sp ,' and attached PS ' ,policyset, ' to SP'
						else :
							print 'Already created SP', sp ,' and attached PS ' ,policyset, ' to SP'
					else : 
						# We are attaching sum PS to default SP of root ConfBlk1.tenant
						sp_desc = "security-profile " 
						custm_attr_list_added = self.custom_attr_add_to_dict(custm_attr_list, custm_attr_list_added)
						response = vnmc.security_profile_modify("", "", "", "", sp, sp_desc, 
policyset, custm_attr_list)
						if "skip" not in response : 
							print 'created SP', sp ,' and attached PS ' ,policyset, ' to SP'
						else :
							print 'Already created SP', sp ,' and attached PS ' ,policyset, ' to SP'   
				ConfBlk1.listed[:] = []


#def main () :
    #SP_link()
    #logger_dbg.debug("Done with creating tenants ,SP and policysets.")

#if __name__ == '__main__' :
    #logger_dbg.debug("Starting to push config to PNSC")
    #main()

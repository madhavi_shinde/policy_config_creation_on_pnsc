# this file creates zones on pnsc
from logger import *
from configfiles import *
import re
import vnmc
import os
class Zone :

	listed = [] 
	number = []
	attr_name = []	
	operator_type = []
	value = []
	conditions = dict()
	tenant=""
	zone_name = ""
	desc = ""
	vdc = ""
	app = ""
	tier = ""
	cond_cri = ""

	def check_vdc_app_tier(self) :
		Zone.tenant = ""
		Zone.vdc = ""
		Zone.app = ""
		Zone.tier = ""
		Zone.listed[0] = re.sub("\\n","",Zone.listed[0])
		Zone.listed[0] = re.sub("\\r","",Zone.listed[0])
		name = Zone.listed[0]
		name = re.sub(".*@","",name)
		name = re.sub(" cond.*","",name)
		entity_list = name.split('/')
		if len(entity_list) == 5 :
			Zone.tenant = entity_list[1]
			Zone.vdc = entity_list[2]
			Zone.app = entity_list[3]
			Zone.tier = entity_list[4]
		elif len(entity_list) == 4:
			Zone.tenant = entity_list[1]
			Zone.vdc = entity_list[2]
			Zone.app = entity_list[3]
		elif len(entity_list) == 3:
			Zone.tenant = entity_list[1]
			Zone.vdc = entity_list[2]
		elif len(entity_list) == 2:
			Zone.tenant = entity_list[1]
		else :
			Zone.tenant = entity_list[0]

	def create_entities(self) :
		Zone.listed[0] = re.sub("\\n","",Zone.listed[0])
		Zone.listed[0] = re.sub("\\r","",Zone.listed[0])
		Zone.cond_cri = Zone.listed[0]
		pattern = re.compile("cond-match-criteria")
		list1 = pattern.findall(Zone.listed[0])
		if len(list1) == 1:
		    Zone.cond_cri = re.sub(".*cond-match-criteria: ","",
Zone.listed[0])
		else :
		    Zone.cond_cri = "match-all"
	
		Zone.zone_name = Zone.listed[0]
		Zone.zone_name = re.sub("zone ","",Zone.zone_name)
		Zone.zone_name = re.sub("@root.*","",Zone.zone_name)
		Zone.zone_name.replace(" ","")

		for line in Zone.listed :
			if "condition" in line :
				values = line.split()
				Zone.number.append(values[1])
				Zone.attr_name.append(values[2])
				Zone.operator_type.append(values[3])
				
				if "@root" in values[4] :  # for obj-grp and zone(member-of)
					values[4] = re.sub("@root.*","",values[4])
				if "vm.custom" in values[2] :
					logger_dbg.debug('custom attr')
					custm_val = re.sub("vm.custom.","",values[2])
					Zone.value.append(custm_val + ' ' + values[4])
				elif len(values) >= 6 :
					Zone.value.append(values[4] + ' ' + values[5])
				else :
					Zone.value.append(values[4])
		Zone.conditions[1] = Zone.number
		Zone.conditions[2] = Zone.attr_name
		Zone.conditions[3] = Zone.operator_type
		Zone.conditions[4] = Zone.value
    


	def zone_seperator(self) :
		with open('configfiles/AllZones.txt','r')  as zone_data :
			for line in zone_data :
				while True :
					try :
						Zone.listed.append(line)
						line = zone_data.next()
						if 'zone ' in line :
							temp = line
							break 
					except StopIteration :
						break
				# got a Zone block
				self.check_vdc_app_tier()
				self.create_entities()
				#logger_dbg.debug('\n\n All values for 1 Zone block= \n')
				#logger_dbg.debug("Zonename=",Zone.zone_name,"tenant=",Zone.tenant, "vdc=",Zone.vdc,"cond_cri=",Zone.cond_cri,"finish")

				#for number1, attr_name1, operator_type1, value1 in zip(Zone.conditions[1],Zone.conditions[2],Zone.conditions[3],Zone.conditions[4]) :
					#logger_dbg.debug(number1, attr_name1, operator_type1, value1)
				
				response = vnmc.zone_create(Zone.vdc, Zone.tenant, Zone.app, Zone.tier,  Zone.zone_name, "zone", Zone.cond_cri, Zone.conditions)
				if "skip" not in response : 
					print 'Zone ',Zone.zone_name,' created'
				else :
					print 'Zone ',Zone.zone_name,'already created ,skipping it'
				Zone.number[:] = []
				Zone.attr_name[:] = []
				Zone.operator_type[:] = []
				Zone.value[:] = []
				Zone.conditions.clear()
				Zone.listed[:] = []
				Zone.listed.append(temp)



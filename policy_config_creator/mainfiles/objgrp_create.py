# this file creates object-groups on pnsc
from logger import *
from configfiles import *
import re
import vnmc
import os
import copy
import cPickle as pickle
class ObjGrp :

	listed = [] 
	number = []
	attr_name = ""	
	operator_type = []
	value = []
	conditions = dict()
	app = ""
	tier = ""
	tenant=""
	obj_name = ""
	desc = ""
	vdc = ""

	def check_tenant_vdc_app_tier(self) :
		ObjGrp.tenant = ""
		ObjGrp.vdc = ""
		ObjGrp.app = ""
		ObjGrp.tier = ""
		name = ObjGrp.listed[0]
		name = re.sub("\\n","",name)
		name = re.sub("\\r","",name)
		name = re.sub(".*@","",name)
		name = re.sub(" .*","",name)
		entity_list = name.split('/')
		if len(entity_list) == 5 :
			ObjGrp.tenant = entity_list[1]
			ObjGrp.vdc = entity_list[2]
			ObjGrp.app = entity_list[3]
			ObjGrp.tier = entity_list[4]
		elif len(entity_list) == 4:
			ObjGrp.tenant = entity_list[1]
			ObjGrp.vdc = entity_list[2]
			ObjGrp.app = entity_list[3]
		elif len(entity_list) == 3:
			ObjGrp.tenant = entity_list[1]
			ObjGrp.vdc = entity_list[2]
		elif len(entity_list) == 2:
			ObjGrp.tenant = entity_list[1]
		else :
			ObjGrp.tenant = entity_list[0]

	def create_entities(self) :
		ObjGrp.listed[0] = re.sub("\\n","",ObjGrp.listed[0])
		ObjGrp.listed[0] = re.sub("\\r","",ObjGrp.listed[0])
		ObjGrp.attr_name = re.sub(".*@root.* ","",ObjGrp.listed[0])
		ObjGrp.ObjGrp = ObjGrp.listed[0]
		ObjGrp.obj_name = ObjGrp.listed[0]
		ObjGrp.obj_name = re.sub("object-group ","",ObjGrp.obj_name)
		ObjGrp.obj_name = re.sub("@root.*","",ObjGrp.obj_name)
		ObjGrp.obj_name.replace(" ","")
		for line in ObjGrp.listed :
			if "match" in line :
				values = line.split()
				ObjGrp.number.append(values[1])
				ObjGrp.operator_type.append(values[2])
				if "@root" in values[3] :  # for obj-grp and zone(member-of)
					values[3] = re.sub("@root.*","",values[3])
				if "net.service" in ObjGrp.attr_name :
					if values[2] == "in-range" or values[2] == "not-in-range" :
						ObjGrp.value.append(values[4] + ' ' + values[6] + ' ' + \
 values[7])
					else :
						if len(values) >= 6 :
							ObjGrp.value.append(values[4] + ' ' + values[6])
						else :
							ObjGrp.value.append(values[4])
				elif "vm.custom" in ObjGrp.attr_name :
					custm_val = re.sub("vm.custom.","",ObjGrp.attr_name)
					ObjGrp.value.append(custm_val + ' ' + values[3])
				elif len(values) >= 5 :
					ObjGrp.value.append(values[3] + ' ' + values[4])
				else :
					ObjGrp.value.append(values[3])
		ObjGrp.conditions[1] = ObjGrp.number
		ObjGrp.conditions[2] = ObjGrp.operator_type
		ObjGrp.conditions[3] = ObjGrp.value
    
	def objgrp_zone_creator(self, objgrp_zone_dict) :
		fp1 = open('picklefile.txt','rb')
		for vdc, tenant, obj_name, app, tier in zip(objgrp_zone_dict[1],objgrp_zone_dict[2],objgrp_zone_dict[3],objgrp_zone_dict[4],objgrp_zone_dict[5]) :
			conditions = pickle.load(fp1)
			response = vnmc.object_group_create(vdc, tenant, app, tier, obj_name, "object group", "zone.name", conditions)
			if "skip" not in response : 
			    print 'Object-group ',ObjGrp.obj_name,' created'
			else :
			    print 'Object-group ',ObjGrp.obj_name,'already created ,skipping it'
		fp1.close()

	def obj_grp_creator(self) :
		tenant_zone = [] 
		vdc_zone = []
		app_zone = []
		tier_zone = []
		obj_name_zone = []
		objgrp_zone_dict = dict()
		fp = open('picklefile.txt','wb')
		with open('configfiles/AllObjectGroups.txt','r')  as obj_data :
			for line in obj_data :
				while True :
					try :
						ObjGrp.listed.append(line)
						line = obj_data.next()
						if 'object-group ' in line :
							temp = line
							break 
					except StopIteration :
						break
				# got a objgrp block
				self.check_tenant_vdc_app_tier()
				self.create_entities()
				#logger_dbg.debug('\n\n All values for 1 ObjGrp block= \n')
				#logger_dbg.debug("ObjGrpname=",ObjGrp.obj_name,"tenant=",ObjGrp.tenant, "vdc=",ObjGrp.vdc,"attr_name=",ObjGrp.attr_name,"finish")
				#for number1, operator_type1, value1 in zip(ObjGrp.conditions[1],ObjGrp.conditions[2],ObjGrp.conditions[3]) :
					#logger_dbg.debug(number1, operator_type1, value1)
				if "zone.name" in ObjGrp.attr_name :
					vdc_zone.append(ObjGrp.vdc)
					tenant_zone.append(ObjGrp.tenant)
					app_zone.append(ObjGrp.app)
					tier_zone.append(ObjGrp.tier)
					obj_name_zone.append(ObjGrp.obj_name)
					#conditions1 = copy.deepcopy(ObjGrp.conditions)
					pickle.dump(ObjGrp.conditions,fp)
				else :
					response = vnmc.object_group_create(ObjGrp.vdc, ObjGrp.tenant, ObjGrp.app, ObjGrp.tier, ObjGrp.obj_name, "object group", ObjGrp.attr_name, ObjGrp.conditions)
					if "skip" not in response : 
						print 'Object-group ',ObjGrp.obj_name,' created'
					else :
						print 'Object-group ',ObjGrp.obj_name,'already created ,skipping it'
				ObjGrp.number[:] = []
				ObjGrp.operator_type[:] = []
				ObjGrp.value[:] = []
				ObjGrp.conditions.clear()
				ObjGrp.listed[:] = []
				ObjGrp.listed.append(temp)

		fp.close()
		objgrp_zone_dict[1] = vdc_zone
		objgrp_zone_dict[2] = tenant_zone
		objgrp_zone_dict[3] = obj_name_zone
		objgrp_zone_dict[4] = app_zone
		objgrp_zone_dict[5] = tier_zone
		return (objgrp_zone_dict)

#def main () :
    #OG1 = ObjGrp()        
    #OG1.obj_grp_seperator()

#if __name__ == '__main__' :
        #main()

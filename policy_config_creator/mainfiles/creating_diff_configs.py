# accepts txt file as input and creates new file which has all security-profiles.
from vsg import variables
import re
from configfiles import *
import os
from logger import *
from vnmc import *

#from vsg.variables import *

def seperate_SPs() :
	with open("configfiles/AllSecurityProfiles.txt","w") as new_data :
		with open("configfiles/ShowRunningConfigData.txt","r") as file_data :
			for line in file_data :
				if 'security-profile' in line :
					new_data.write(line)
					for line in file_data :
						lists = ['object-group ','zone ','rule '] 
						if any(x in line for x in lists) : 
							return None
						else : 
							new_data.write(line)
	logger_dbg.debug('finish')

def seperate_show_running_config() :
	tech_support = raw_input('Please enter name of your tech-support file : ')
	with open("configfiles/ShowRunningConfigData.txt","w") as config_data :
		if os.path.isfile(tech_support) :        
			with open(tech_support,"r") as file_data :
				for line in file_data :
					if 'show running-config' in line :
						config_data.write(line)
						for line in file_data :
							if 'vnm-policy-agent' in line : 
								return vnmc_ip, vnmc_user, vnmc_pass
							else : 
								config_data.write(line)
		else :
			flag = True
			filename = tech_support
			filename = re.sub(".*/","",filename)
			filepath = tech_support
			filepath = re.sub('/'+filename,"",filepath)
			print filename,"at",filepath,"not found"
			exit()
	return vnmc_ip, vnmc_user, vnmc_pass

def seperate_object_groups() :
	with open('configfiles/AllObjectGroups.txt','w') as new_data :
		with open('configfiles/ShowRunningConfigData.txt','r') as file_data :
			for line in file_data :
				if 'object-group' in line :
					new_data.write(line)
					for line in file_data :
						lists = ['zone ','rule ','Policy ','policy '] 
						if any(x in line for x in lists) : 
							return None
						else : 
							new_data.write(line)

def seperate_zones() :
	with open('configfiles/AllZones.txt','w') as zone_data :
		with open('configfiles/ShowRunningConfigData.txt','r') as file_data :
			for line in file_data :
				if 'zone ' in line :
					zone_data.write(line)
					for line in file_data :
						lists = ['rule ','Policy ','policy '] 
						if any(x in line for x in lists) :
							return None
						else : 
							zone_data.write(line)

def seperate_rules() :
	with open('configfiles/AllRules.txt','w') as rule_data :
		with open('configfiles/ShowRunningConfigData.txt','r') as file_data :
			for line in file_data :
				if 'rule ' in line :
					rule_data.write(line)
					for line in file_data :
						lists = ['Policy ','policy '] 
						if any(x in line for x in lists) :
							return None
						else : 
							rule_data.write(line)

def seperate_policies() :
	with open('configfiles/AllPolicies.txt','w') as policy_data :
		with open('configfiles/ShowRunningConfigData.txt','r') as file_data :
			for line in file_data :
				if 'Policy ' in line :
					policy_data.write(line)
					for line in file_data :
						policy_data.write(line)
	logger_dbg.debug('finish with policies')

def main () :
    #seperate show running-config
	seperate_show_running_config()
    #seperate out SPs
	seperate_SPs()
    #seperate_tenants()
    #seperate_object_groups()
    #seperate_zones()
    #seperate_rules()
    #seperate_policies()
    #logger_dbg.debug('done with code :):):)')

if __name__ == '__main__' :
	main()

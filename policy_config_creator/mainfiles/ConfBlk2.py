# This program creates policies and rules
from logger import *
from configfiles import *
import re
import vnmc
import os

class ConfBlk2 :

	listed = [] 
	number = []
	order = []
	attr_name = []	
	operator_type = []
	value = []
	conditions = dict()
	cond_cri="match-all"
	rule=""
	policy=""
	tenant=""
	action_type=""
	desc = ""
	vdc = ""
	app = ""
	tier = ""
	main_order = {}
	pol_rule = ""
	rule_order = 0
	
	def check_vdc_app_tier(self) :
		ConfBlk2.tenant = ""
		ConfBlk2.vdc = ""
		ConfBlk2.app = ""
		ConfBlk2.tier = ""
		ConfBlk2.listed[0] = re.sub("\\n","",ConfBlk2.listed[0])
		ConfBlk2.listed[0] = re.sub("\\r","",ConfBlk2.listed[0])
		name = ConfBlk2.listed[0]
		name = re.sub(".*@","",name)
		name = re.sub(" cond.*","",name)
		entity_list = name.split('/')
		if len(entity_list) == 5 :
			ConfBlk2.tenant = entity_list[1]
			ConfBlk2.vdc = entity_list[2]
			ConfBlk2.app = entity_list[3]
			ConfBlk2.tier = entity_list[4]
		elif len(entity_list) == 4:
			print 'inside 4' 
			ConfBlk2.tenant = entity_list[1]
			print 't=',ConfBlk2.tenant
			ConfBlk2.vdc = entity_list[2]
			print 'v=',ConfBlk2.vdc
			ConfBlk2.app = entity_list[3]
		elif len(entity_list) == 3:
			ConfBlk2.tenant = entity_list[1]
			ConfBlk2.vdc = entity_list[2]
		elif len(entity_list) == 2:
			ConfBlk2.tenant = entity_list[1]
		else :
			ConfBlk2.tenant = entity_list[0]

	# For mainitaing uniqeness ,we are storing policy & rule name together instead 
#of only rule name
	def sep_order_from_policy(self) :
		with open('configfiles/AllPolicies.txt','r')  as policy_data :
			for line in policy_data :
				if "rule " in line :
					line = re.sub("\\n","",line)
					line = re.sub("\\r","",line)
					ConfBlk2.pol_rule = line
					ConfBlk2.pol_rule = re.sub("@root.*","",ConfBlk2.pol_rule)
					ConfBlk2.pol_rule = re.sub("  rule ","",ConfBlk2.pol_rule)
					ConfBlk2.rule_order = line
					ConfBlk2.rule_order = re.sub(".*order ","",ConfBlk2.rule_order)
					ConfBlk2.rule_order = re.sub(" ","",ConfBlk2.rule_order)
					ConfBlk2.main_order.update({ConfBlk2.pol_rule : ConfBlk2.rule_order})                    
            #print "dict = ",ConfBlk2.main_order

	def create_entities(self) :
		logger_dbg.debug('\n\nlist = %s' % ConfBlk2.listed)
		ConfBlk2.listed[0] = re.sub("\\n","",ConfBlk2.listed[0])
		ConfBlk2.listed[0] = re.sub("\\r","",ConfBlk2.listed[0])
		pattern = re.compile("cond-match-criteria")
		list1 = pattern.findall(ConfBlk2.listed[0])
		if len(list1) == 1:
			ConfBlk2.cond_cri = re.sub(".*cond.* ","",ConfBlk2.listed[0])
		else :
			ConfBlk2.cond_cri = "match-all"
		ConfBlk2.rule = ConfBlk2.listed[0]
		ConfBlk2.rule = re.sub("@root.*","",ConfBlk2.rule)
		ConfBlk2.rule = re.sub(".*/","",ConfBlk2.rule)
		ConfBlk2.rule.replace(" ","")
		ConfBlk2.policy = ConfBlk2.listed[0]
		ConfBlk2.policy = re.sub("rule ","",ConfBlk2.policy)
		ConfBlk2.policy = re.sub("/.*","",ConfBlk2.policy)
		ConfBlk2.pol_rule = ConfBlk2.listed[0]
		ConfBlk2.pol_rule = re.sub("@root.*","",ConfBlk2.pol_rule)
		ConfBlk2.pol_rule = re.sub("rule ","",ConfBlk2.pol_rule)
		ConfBlk2.rule_order = ConfBlk2.main_order.get(ConfBlk2.pol_rule)
		for line in ConfBlk2.listed :
			if "condition" in line :
				values = line.split()
				ConfBlk2.number.append(values[1])
				ConfBlk2.order.append(values[1])
				ConfBlk2.attr_name.append(values[2])
				ConfBlk2.operator_type.append(values[3])
				if "@root" in values[4] :  # for obj-grp and zone
					values[4] = re.sub("@root.*","",values[4])
				if "service" in values[2] :
					if len(values) == 6 :
						ConfBlk2.value.append(values[5])
					elif len(values) == 8 :
						ConfBlk2.value.append(values[5] + ' ' + values[7])
					elif len(values) == 9 :
						ConfBlk2.value.append(values[5] + ' ' + values[7] + ' ' + \
 values[8])
					else :
						ConfBlk2.value.append(values[4])
				elif "protocol" in values[2] :
					if len(values) == 6 :
						ConfBlk2.value.append(values[4] + ' ' + values[5])
					else :
						ConfBlk2.value.append(values[4])
				elif "vm.custom" in values[2] :
					custm_val = re.sub(".*custom.","",values[2])
					ConfBlk2.value.append(custm_val + ' ' + values[4])
				elif "in-range" in values[3] or "not-in-range" in values[3]:       
                    # this is for ethertype,net.port,ip-address,protocol
					ConfBlk2.value.append(values[4] + ' ' + values[5])
				elif "ip-address" in values[2] :
					if "prefix" in values[3] :
						ConfBlk2.value.append(values[4] + ' ' + values[5])
					else :
						ConfBlk2.value.append(values[4])
				else:
					ConfBlk2.value.append(values[4])

			if "action" in line :
				values = line.split()
				ConfBlk2.action_type = values[1]
		
		ConfBlk2.conditions[1] = ConfBlk2.number
		ConfBlk2.conditions[2] = ConfBlk2.order
		ConfBlk2.conditions[3] = ConfBlk2.attr_name
		ConfBlk2.conditions[4] = ConfBlk2.operator_type
		ConfBlk2.conditions[5] = ConfBlk2.value
    

	def policy_rule_link(self) :
		policies_created = []
		self.sep_order_from_policy()
		with open('configfiles/AllRules.txt','r')  as rule_data :
			for line in rule_data :
				while True :
					try :
						ConfBlk2.listed.append(line)
						line = rule_data.next()
						if 'rule ' in line :
							temp = line
							break 
					except StopIteration :
						break
				# got a rule block
				self.check_vdc_app_tier()
				self.create_entities()
				#logger_dbg.debug('\n\n All values for 1 rule block= \n')
				#logger_dbg.debug("rulename=",ConfBlk2.rule,"policy=",ConfBlk2.policy, "tenant=",ConfBlk2.tenant, "cond_criteria=",ConfBlk2.cond_cri,"action_type=",ConfBlk2.action_type,
#   "main_order=",ConfBlk2.main_order,"vdc=",ConfBlk2.vdc)

				#for number1, order1, attr_name1, operator_type1, value1 in zip( \
#ConfBlk2.conditions[1],ConfBlk2.conditions[2],ConfBlk2.conditions[3],ConfBlk2.conditions[4] \
#,ConfBlk2.conditions[5]) :
					#logger_dbg.debug(number1, order1, attr_name1, operator_type1, value1)
				if not policies_created :
				#list is empty
					if ConfBlk2.policy != "default" :
						response = vnmc.policy_create(ConfBlk2.vdc, ConfBlk2.tenant, 
ConfBlk2.app, ConfBlk2.tier, ConfBlk2.policy, ConfBlk2.desc)	
						if "skip" not in response : 
							print 'Policy ',ConfBlk2.policy,' created'
						else :
							print 'Policy ',ConfBlk2.policy,'already created ,skipping it'
					else :
						pass
					policies_created.append(ConfBlk2.policy)
				if ConfBlk2.policy in policies_created :
					if ConfBlk2.rule != "default-rule" :				
						response = vnmc.policy_modify(ConfBlk2.vdc, ConfBlk2.tenant, ConfBlk2.app,  ConfBlk2.tier, ConfBlk2.policy, ConfBlk2.rule, ConfBlk2.rule_order, ConfBlk2.desc,ConfBlk2.action_type,
ConfBlk2.conditions, ConfBlk2.cond_cri)
						if "skip" not in response : 
							print 'Rule ',ConfBlk2.rule,' created for policy ',ConfBlk2.policy
						else :
							print 'Rule ',ConfBlk2.rule,'already created for policy ',ConfBlk2.policy,',skipping it'
					else :
						pass
				if ConfBlk2.policy not in policies_created :
					if ConfBlk2.policy != "default" :
						response = vnmc.policy_create(ConfBlk2.vdc, ConfBlk2.tenant, 
ConfBlk2.app, ConfBlk2.tier,ConfBlk2.policy, ConfBlk2.desc)
						if "skip" not in response : 
							print 'Policy ',ConfBlk2.policy,' created'
						else :
							print 'Policy ',ConfBlk2.policy,'already created ,skipping it'
					else :
						pass
					policies_created.append(ConfBlk2.policy) 
					if ConfBlk2.rule != "default-rule" :
						response = vnmc.policy_modify(ConfBlk2.vdc, ConfBlk2.tenant, 
ConfBlk2.app, ConfBlk2.tier,ConfBlk2.policy, ConfBlk2.rule, ConfBlk2.rule_order, ConfBlk2.desc,
ConfBlk2.action_type, ConfBlk2.conditions, ConfBlk2.cond_cri)
						if "skip" not in response : 
							print 'Rule ',ConfBlk2.rule,' created for policy ',ConfBlk2.policy
						else :
							print 'Rule ',ConfBlk2.rule,'already created for policy ',ConfBlk2.policy,',skipping it'
					else :
						pass
						logger_dbg.debug ('rule default-rule is already there')
                
				ConfBlk2.number[:] = []
				ConfBlk2.order[:] = []
				ConfBlk2.attr_name[:] = []
				ConfBlk2.operator_type[:] = []
				ConfBlk2.value[:] = []
				ConfBlk2.conditions.clear()
				ConfBlk2.listed[:] = []
				ConfBlk2.listed.append(temp)


#def main () :
    #R1 = ConfBlk2()        
    #R1.rule_link()

#if __name__ == '__main__' :
        #main()

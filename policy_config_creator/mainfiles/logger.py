import logging

LOG_FILENAME = 'error_log.log'
logger_err = logging.getLogger('error_log')
handler = logging.FileHandler('outputfiles/error_log.log','w')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger_err.addHandler(handler)
logger_err.setLevel(logging.ERROR)

LOG_FILENAME = 'debug_log.log'
logger_dbg = logging.getLogger('debug_log')
handler = logging.FileHandler('outputfiles/debug_log.log','w')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
handler.setFormatter(formatter)
logger_dbg.addHandler(handler)
logger_dbg.setLevel(logging.DEBUG)


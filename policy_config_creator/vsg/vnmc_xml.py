import os
import re
import StringIO
import datetime
from mainfiles.logger import *
import requests
import variables

def print_set_header(output, cookie):
    output.write("\n<configConfMos \n")
    output.write("cookie=%s> \n" % cookie)
    output.write(" <inConfigs> \n")

def print_set_footer(output):
    output.write("\n  </inConfigs> \n")
    output.write("</configConfMos>\n")

def print_set_header_zone(output, cookie):
    output.write("\n<configConfMo \n")
    output.write("cookie=%s> \n" % cookie)
    output.write(" <inConfig> \n")

def print_set_footer_zone(output):
    output.write("\n  </inConfig> \n")
    output.write("</configConfMo>\n")

def print_vdc(set_mode, output, tenant, vdc, vdc_desc):
    dn = "org-root/org-" + tenant + "/org-" + vdc
    if set_mode == "yes":
	output.write("\n <orgDatacenter descr=\"%s\" dn=\"%s\" name=\"%s\" \
status=\"created\"/>\n" %(vdc_desc, dn, vdc))

def print_application(set_mode, output, tenant, vdc, app, app_desc):
    dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app
    if set_mode == "yes":
	output.write("\n <orgApp descr=\"%s\" dn=\"%s\" name=\"%s\" \
status=\"created\"/>\n" %(app_desc, dn, app))

def print_tier(set_mode, output, tenant, vdc, app, tier, tier_desc):
    dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + tier
    if set_mode == "yes":
	output.write("\n <orgTier descr=\"%s\" dn=\"%s\" name=\"%s\" \
status=\"created\"/>\n" %(tier_desc, dn, tier))

def print_zone(set_mode, output, vdc, tenant, app, tier, zonename, desc, match_cond, conditions):
    if tier != "":
        zone_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + tier + "/zone-" + zonename
    elif app != "":
        zone_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/zone-" + zonename
    elif vdc != "":
        zone_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/zone-" + zonename
    elif tenant != "":
        zone_dn_name = "org-root/org-" + tenant + "/zone-" + zonename
    else :
        zone_dn_name = "org-root/zone-" + zonename

    if set_mode == "yes":
	output.write("<pair key=\"%s\"> \n" % zone_dn_name)
        output.write("<policyZone descr=\"%s\" dn=\"%s\" name=\"%s\" \
matchCriteria= \"%s\" />\n" % (desc, zone_dn_name, zonename, match_cond))
	output.write("</pair>\n")

	for condition_number, attr_name, operator_type, value in zip(conditions[1],
conditions[2],conditions[3],conditions[4]) : 
	    print_zone_condition(set_mode, output, vdc, tenant, app, tier, zonename, 
condition_number, attr_name, operator_type, value)
    else:
        output.write("<pair key=\"%s\">\n" % (zone_dn_name))
        output.write("    <policyZone dn=\"%s\" status=\"deleted\"/>\n" % 
(zone_dn_name))
        output.write("</pair>\n")

def print_tenant(set_mode, output, cookie, name, desc):
    output.write("\n<configConfMo\n")
    output.write("dn=\"\"\n")
    output.write("cookie=%s> \n" % cookie)
    output.write("inHierarchical=\"false\">\n")
    output.write("    <inConfig> \n")
    output.write("<orgTenant\n")
    output.write("\n")
    if set_mode == "yes":
        output.write("descr=\"%s\"\n" % desc)
    output.write("dn=\"org-root/org-%s\"\n" % name)
    output.write("name=\"%s\"\n" % name)
    #output.write("\n")
    if set_mode == "yes":
        output.write("status=\"created\"/>\n")
    else:
        output.write("status=\"deleted\"/>\n")
    output.write("\n  </inConfig> \n")
    output.write("</configConfMo>\n")

def print_rules_get_request(output, cookie, tenant, sp_name):
    policy_name = sp_name + "-PS-P"
    if tenant == "":
        policy_dn_name = "org-root/" + "pol-" + policy_name
    else:
        policy_dn_name = "org-root/" + tenant + "/pol-" + policy_name
    output.write("\n")
    output.write("<configResolveChildren\n")
    output.write("cookie=%s\n" % cookie)
    output.write("\n")
    output.write("\n")
    output.write("inDn=\"%s\"\n" % policy_dn_name)
    output.write("inHierarchical=\"true\">\n")
    output.write("    <inFilter>\n")
    output.write("    </inFilter>\n")
    output.write("</configResolveChildren>\n")
    
def print_conds_get_request(output, cookie, tenant, sp_name, rule_name):
    policy_name = sp_name + "-PS-P"
    if tenant == "":
        rule_dn_name = "org-root/pol-" + policy_name + "/rule-" + rule_name
    else:
        rule_dn_name = "org-root/" + tenant + "/pol-" + policy_name + "/rule-" \
 + rule_name  
    output.write("\n")
    output.write("<configResolveChildren\n")
    output.write("cookie=%s\n" % cookie)
    output.write("\n")
    output.write("\n")
    output.write("inDn=\"%s\"\n" % rule_dn_name)
    output.write("inHierarchical=\"true\">\n")
    output.write("    <inFilter>\n")
    output.write("    </inFilter>\n")
    output.write("</configResolveChildren>\n")

def print_ogroup_get_request(output, cookie, tenant):
    if tenant == "":
        dn_name = "org-root"
    else:
        dn_name = "org-root/" + tenant  
    output.write("\n")
    output.write("<configResolveChildren\n")
    output.write("cookie=%s\n" % cookie)
    output.write("\n")
    output.write("\n")
    output.write("classId=\"policyObjectGroup\"\n")
    output.write("inDn=\"%s\"\n" % dn_name)
    output.write("inHierarchical=\"false\">\n")
    output.write("    <inFilter>\n")
    output.write("    </inFilter>\n")
    output.write("</configResolveChildren>\n")

def print_zone_get_request(output, cookie, tenant):
    if tenant == "":
        dn_name = "org-root"
    else:
        dn_name = "org-root/" + tenant
    output.write("\n")
    output.write("<configResolveChildren\n")
    output.write("cookie=%s\n" % cookie)
    output.write("\n")
    output.write("\n")
    output.write("classId=\"policyZone\"\n")
    output.write("inDn=\"%s\"\n" % dn_name)
    output.write("inHierarchical=\"false\">\n")
    output.write("    <inFilter>\n")
    output.write("    </inFilter>\n")
    output.write("</configResolveChildren>\n")

def print_zone_conds_get_request(output, cookie, tenant, zone_name):
    if tenant == "":
        dn_name = "org-root/zone-" + zone_name
    else:
        dn_name = "org-root/" + tenant + "/zone-" + zone_name
    output.write("\n")
    output.write("<configResolveChildren\n")
    output.write("cookie=%s\n" % cookie)
    output.write("\n")
    output.write("\n")
    output.write("inDn=\"%s\"\n" % dn_name)
    output.write("inHierarchical=\"true\">\n")
    output.write("    <inFilter>\n")
    output.write("    </inFilter>\n")
    output.write("</configResolveChildren>\n")

def print_ogroup_conds_get_request(output, cookie, tenant, ogroup_name):
    if tenant == "":
        dn_name = "org-root/objgrp-" + ogroup_name
    else:
        dn_name = "org-root/" + tenant + "/objgrp-" + ogroup_name
    output.write("\n")
    output.write("<configResolveChildren\n")
    output.write("cookie=%s\n" % cookie)
    output.write("\n")
    output.write("\n")
    output.write("inDn=\"%s\"\n" % dn_name)
    output.write("inHierarchical=\"true\">\n")
    output.write("    <inFilter>\n")
    output.write("    </inFilter>\n")
    output.write("</configResolveChildren>\n")

def print_sp_get_request(output, cookie, tenant):
    if not tenant:
        sp_dn_name = "org-root"
    else:
        sp_dn_name = "org-root/" + tenant
    output.write("\n")
    output.write("<configResolveChildren\n")
    output.write("cookie=%s\n" % cookie)
    output.write("\n")
    output.write("classId=\"policyVirtualNetworkServiceProfile\"\n")
    output.write("inDn=\"%s\"\n" % sp_dn_name)
    output.write("inHierarchical=\"false\">\n")
    output.write("    <inFilter>\n")
    output.write("    </inFilter>\n")
    output.write("</configResolveChildren>\n")

def print_zone_condition(set_mode, output, vdc, tenant, app, tier, zonename, 
condition_number, attr_name, operator_type, value):
    if tier != "":
        zone_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + tier + "/zone-" + zonename
    elif app != "":
        zone_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/zone-" + zonename
    elif vdc != "":
        zone_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/zone-" + zonename
    elif tenant != "":
        zone_dn_name = "org-root/org-" + tenant + "/zone-" + zonename
    else :
        zone_dn_name = "org-root/zone-" + zonename

    if set_mode == "yes":
        if "custom" in attr_name:
            attr_expression = "policyZoneCustomExpression"
	
	    vnmc_operator = variables.vnmc_operator_equivalent[operator_type]
            output.write("<pair key=\"%s/zone-cond-%s/zone-cust-expr2\">\n" \
% (zone_dn_name, condition_number))
            output.write("          <%s dn=\"%s/zone-cond-%s/zone-cust-expr2\" \
id=\"2\" opr=\"%s\"/>\n" % (attr_expression, zone_dn_name, condition_number, 
vnmc_operator))
            output.write("</pair>\n")
        else:
            attr_expression = "policyZoneExpression"

            vnmc_operator = variables.vnmc_operator_equivalent[operator_type]
            output.write("<pair key=\"%s/zone-cond-%s/zone-expr2\">\n" % \
(zone_dn_name, condition_number))
            output.write("          <%s dn=\"%s/zone-cond-%s/zone-expr2\" \
id=\"2\" opr=\"%s\"/>\n" % (attr_expression, zone_dn_name, condition_number,\
 vnmc_operator))
            output.write("</pair>\n")

        if "ip-address" in attr_name:
            
            if operator_type == "in-range" or operator_type == "not-in-range":
                range_value = value.split()
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/nw-ip-2\">\n" \
% (zone_dn_name, condition_number))
                output.write("           <policyIPAddress dn=\"%s/zone-cond-%s \
/zone-expr2/nw-ip-2\" id=\"2\" placement=\"begin\" value=\"%s\" />\n" % \
(zone_dn_name, condition_number, range_value[0]))
                output.write("</pair>\n")
                output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/nw-ip-3\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyIPAddress dn=\"%s/zone-cond-%s/ \
zone-expr2/nw-ip-3\" id=\"3\" placement=\"end\" value=\"%s\" />\n" % (zone_dn_name, condition_number, range_value[1]))
                output.write("</pair>\n")
            elif operator_type == "prefix":
                prefix_value = value.split()
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/nw-ip-2\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyIPAddress dn=\"%s/zone-cond-%s/ \
zone-expr2/nw-ip-2\" id=\"2\" value=\"%s\" />\n" % (zone_dn_name, condition_number, prefix_value[0]))
                output.write("</pair>\n")
                output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/nw-ip-subnet2 \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyIPSubnet dn=\"%s/zone-cond-%s/ \
zone-expr2/nw-ip-subnet2\" id=\"2\" value=\"%s\" />\n" % (zone_dn_name, 
condition_number, prefix_value[1]))
                output.write("</pair>\n")
	    elif operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyIPAddress\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" objgrpName= \
\"%s\" />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
            else:
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/nw-ip-2\">\n" \
% (zone_dn_name, condition_number))
                output.write("           <policyIPAddress dn=\"%s/zone-cond-%s/ \
zone-expr2/nw-ip-2\" id=\"2\" value=\"%s\" />\n" % (zone_dn_name, 
condition_number, value))
                output.write("</pair>\n")
        
        if "vm.name" in attr_name:
	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyInstName\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" objgrpName=\"%s\" \
 />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
	    else:
            	output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/compute-inst-2 \
\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyInstName dn=\"%s/zone-cond-%s/zone-expr2/ \
compute-inst-2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name, condition_number,
 value))
            	output.write("</pair>\n")

	if "vm.cluster-name" in attr_name:
	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyClusterName\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" objgrpName= \
\"%s\" />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
	    else:
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/compute- \
cluster-2\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyInstName dn=\"%s/zone-cond-%s/zone-expr2/ \
compute-cluster-2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name, condition_number,
 value))
            	output.write("</pair>\n")

	if "vm.os-fullname" in attr_name:
	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyGuestOSFullName\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" objgrpName \
=\"%s\" />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
	    else:
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/ \
compute-osfname-2\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyGuestOSFullName dn=\"%s/zone-cond-%s \
/zone-expr2/compute-osfname-2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name, condition_number, value))
            	output.write("</pair>\n")

	if "vm.host-name" in attr_name:
	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyHypervisorName\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" objgrpName \
=\"%s\" />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
	    else:
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/compute- \
hypervisor-2\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyHypervisorName dn=\"%s/zone-cond-%s/ \
zone-expr2/compute-hypervisor-2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name, condition_number, value))
            	output.write("</pair>\n")

	if "vm.vapp-name" in attr_name:
	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyParentAppName\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" objgrpName= \
\"%s\" />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
	    else:
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/ \
compute-app-2\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyParentAppName dn=\"%s/zone-cond-%s/ \
zone-expr2/compute-app-2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name,
 condition_number, value))
            	output.write("</pair>\n")

        if "vm.portprofile-name" in attr_name:
	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyPortProfileName\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" \
objgrpName=\"%s\" />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
	    else:
            	output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/compute- \
ppname-2\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyPortProfileName dn=\"%s/zone-cond-%s/ \
zone-expr2/compute-ppname-2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name, 
condition_number, value))
            	output.write("</pair>\n")

	if "vm.resource-pool" in attr_name:
	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyResourcePool\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" objgrpName= \
\"%s\" />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
	    else:
            	output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/compute- \
res-pool-2\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyResourcePool dn=\"%s/zone-cond-%s/ \
zone-expr2/compute-res-pool-2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name, 
condition_number, value))
            	output.write("</pair>\n")

	if "vm.os-hostname" in attr_name:
	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/objgrp-ref \
\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyHostname\" dn=\"%s/zone-cond-%s/zone-expr2/objgrp-ref\" \
objgrpName=\"%s\" />\n" % (zone_dn_name, condition_number, value))
                output.write("</pair>\n")
	    else:
            	output.write("<pair key=\"%s/zone-cond-%s/zone-expr2/ \
compute-hostname-2\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyHostname dn=\"%s/zone-cond-%s/ \
zone-expr2/compute-hostname-2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name, condition_number, value))
            	output.write("</pair>\n")

	if "vm.custom" in attr_name:
	    values = value.split()
	    output.write("<pair key=\"%s/zone-cond-%s/zone-cust-expr2/attr-ref \
\">\n" % (zone_dn_name, condition_number))
            output.write("    <policyAttributeDesignator dn=\"%s/zone-cond-%s/ \
zone-cust-expr2/attr-ref\"  attrName=\"%s\" dictionary=\"SecurityProfileCustom\
Attribute\" />\n" % (zone_dn_name, condition_number, values[0]))
            output.write("</pair>\n")

	    if operator_type == "member-of" or operator_type == "not-member-of":
		output.write("<pair key=\"%s/zone-cond-%s/zone-cust-expr2/ \
objgrp-ref\">\n" % (zone_dn_name, condition_number))
                output.write("           <policyObjectGroupNameRef attrClassName=\"policyAttributeDesignator\" dn=\"%s/zone-cond-%s/zone-cust-expr2/objgrp-ref\" \
objgrpName=\"%s\" />\n" % (zone_dn_name, condition_number, values[1]))
                output.write("</pair>\n")
	    else:
            	output.write("<pair key=\"%s/zone-cond-%s/zone-cust-expr2/ \
attr-val2\">\n" % (zone_dn_name, condition_number))
            	output.write("    <policyAttributeValue dn=\"%s/zone-cond-%s/ \
zone-cust-expr2/attr-val2\"  id=\"2\" value=\"%s\" />\n" % (zone_dn_name,
 condition_number, values[1]))
            	output.write("</pair>\n")	

        output.write("<pair key=\"%s/zone-cond-%s\">\n" % (zone_dn_name, 
condition_number))
        output.write("           <policyZoneCondition dn=\"%s/zone-cond-%s\" \
id=\"%s\" status=\"created\" />\n" % (zone_dn_name, condition_number, 
condition_number))
        output.write("</pair>")

    else:
        output.write("<pair key=\"%s/zone-cond-%s\">\n" % (zone_dn_name, 
condition_number))
        output.write("          <policyZoneCondition dn=\"%s/zone-cond-%s\" id= \
\"%s\" status=\"deleted\"/>\n" % (zone_dn_name, condition_number, condition_number))
        output.write("</pair>\n")

def print_objgrp_condition(set_mode, output, vdc, tenant, app, tier, ogroupname, 
condition_number, attr_name, operator_type, value):
    if tier != "":
        ogroup_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + tier + "/objgrp-" + ogroupname
    elif app != "":
        ogroup_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/objgrp-" + ogroupname
    elif vdc != "":
        ogroup_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/objgrp-" + ogroupname
    elif tenant != "":
        ogroup_dn_name = "org-root/org-" + tenant + "/objgrp-" + ogroupname
    else :
        ogroup_dn_name = "org-root/objgrp-" + ogroupname

    if set_mode == "yes":
        if "custom" in attr_name:
            attr_expression = "policyObjectGroupCustomExpression"

	    vnmc_operator = variables.vnmc_operator_equivalent[operator_type]
            output.write("<pair key=\"%s/objgrp-cust-expr-%s\">\n" % (ogroup_dn_name, condition_number))
            output.write("          <%s dn=\"%s/objgrp-cust-expr-%s\" id=\"%s\" \
opr=\"%s\"/>\n" % (attr_expression, ogroup_dn_name, condition_number, 
condition_number, vnmc_operator))
            output.write("</pair>\n")
		
        else:
            attr_expression = "policyObjectGroupExpression"
    
            vnmc_operator = variables.vnmc_operator_equivalent[operator_type]
            output.write("<pair key=\"%s/objgrp-expr-%s\">\n" % (ogroup_dn_name, condition_number))
            output.write("          <%s dn=\"%s/objgrp-expr-%s\" id=\"%s\" opr= \
\"%s\"/>\n" % (attr_expression, ogroup_dn_name, condition_number, condition_number, vnmc_operator))
            output.write("</pair>\n")

	if "vm.custom" in attr_name:
	    values = value.split()  # value should be (color val123) from vm.custom.color
	    output.write("<pair key=\"%s/objgrp-cust-expr-%s/attr-ref\">\n" % 
(ogroup_dn_name, condition_number))
            output.write("<policyAttributeDesignator attrName=\"%s\"  dictionary=\"SecurityProfileCustomAttribute\" dn=\"%s/objgrp-cust-expr-%s/attr-ref\" />\n" \
% (values[0], ogroup_dn_name, condition_number))
            output.write("</pair>\n")
		
	    output.write("<pair key=\"%s/objgrp-cust-expr-%s/attr-val0\">\n" % (ogroup_dn_name, condition_number))
            output.write("<policyAttributeValue dn=\"%s/objgrp-cust-expr-%s \
/attr-val0\" id=\"0\" status=\"created\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, values[1]))
            output.write("</pair>\n")
	    

	elif "ip-address" in attr_name:
            output.write("<pair key=\"%s/objgrp-expr-%s/nw-ip-2\">\n" %
(ogroup_dn_name, condition_number))
            if operator_type == "in-range" or operator_type == "not-in-range":
                range_value = value.split() #'-'
                output.write("           <policyIPAddress dn=\"%s/objgrp-expr-%s/nw-ip-2\" id=\"2\" placement=\"begin\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, range_value[0]))
                output.write("</pair>\n")
                output.write("<pair key=\"%s/objgrp-expr-%s/nw-ip-3\">\n" % (ogroup_dn_name, condition_number))
                output.write("           <policyIPAddress dn=\"%s/objgrp-expr-%s/nw-ip-3\" id=\"22\" placement=\"end\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, range_value[1]))
                output.write("</pair>\n")
            elif operator_type == "prefix":
                prefix_value = value.split() #'-'
		if len(prefix_value) == 1 :
		    prefix_value[1] = ""
                output.write("           <policyIPAddress dn=\"%s/objgrp-expr-%s/nw-ip-2\" id=\"2\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, prefix_value[0]))
                output.write("</pair>\n")
                output.write("<pair key=\"%s/objgrp-expr-%s/nw-ip-subnet2\">\n" % (ogroup_dn_name, condition_number))
                output.write("           <policyIPSubnet dn=\"%s/objgrp-expr-%s/nw-ip-subnet2\" id=\"2\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, prefix_value[1]))
                output.write("</pair>\n")
            else:
                output.write("           <policyIPAddress dn=\"%s/ogroup-expr-%s/nw-ip-2\" id=\"2\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, value))
                output.write("</pair>\n")

	elif "protocol" in attr_name:
	    if operator_type == "in-range"  or operator_type == "not-in-range":
		range_value = value.split()
		output.write("<pair key=\"%s/objgrp-expr-%s/nw-protocol-2\">\n" % (ogroup_dn_name, condition_number))
            	output.write("<policyProtocol dn=\"%s/objgrp-expr-%s/nw-protocol-2\" id=\"2\"  placement=\"begin\" value=%s />\n" % (ogroup_dn_name, condition_number, range_value[0]))
            	output.write("</pair>\n")
 
		output.write("<pair key=\"%s/objgrp-expr-%s/nw-protocol-3\">\n" % (ogroup_dn_name, condition_number))
            	output.write("<policyProtocol dn=\"%s/objgrp-expr-%s/nw-protocol-3\" id=\"3\"  placement=\"end\" value=%s />\n" % (ogroup_dn_name, condition_number, range_value[1]))
            	output.write("</pair>\n")
	    else :
            	output.write("<pair key=\"%s/objgrp-expr-%s/nw-protocol-2\">\n" % (ogroup_dn_name, condition_number))
            	output.write("<policyProtocol dn=\"%s/objgrp-expr-%s/nw-protocol-2\" id=\"2\" value=%s />\n" % (ogroup_dn_name, condition_number, value))
            	output.write("</pair>\n")
            
	elif "port" in attr_name:
            output.write("<pair key=\"%s/objgrp-expr-%s/nw-port-2\">\n" % (ogroup_dn_name, condition_number))
            if operator_type == "in-range"  or operator_type == "not-in-range":
                range_value = value.split()
                output.write("           <policyNetworkPort dn=\"%s/objgrp-expr-%s/nw-port-2\" id=\"2\" placement=\"begin\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, range_value[0]))
                output.write("</pair>\n")
                output.write("<pair key=\"%s/objgrp-expr-%s/nw-port-3\">\n" % (ogroup_dn_name, condition_number))
                output.write("           <policyNetworkPort dn=\"%s/objgrp-expr-%s/nw-port-3\" id=\"22\" placement=\"end\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, range_value[1]))
                output.write("</pair>\n")
            else:
                output.write("           <policyNetworkPort dn=\"%s/objgrp-expr-%s/nw-port-2\" id=\"3\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, value))
                output.write("</pair>\n")
	
	elif "ethertype" in attr_name:
	    if operator_type == "in-range"  or operator_type == "not-in-range":
		range_value = value.split()
		output.write("<pair key=\"%s/objgrp-expr-%s/nw-ethtype-2\">\n" % (ogroup_dn_name, condition_number))
            	output.write("<policyEthertype dn=\"%s/objgrp-expr-%s/nw-ethtype-2\" id=\"2\"   placement=\"begin\" value=%s />\n" % (ogroup_dn_name, condition_number, range_value[0]))
            	output.write("</pair>\n")
		
		output.write("<pair key=\"%s/objgrp-expr-%s/nw-ethtype-3\">\n" % (ogroup_dn_name, condition_number))
            	output.write("<policyEthertype dn=\"%s/objgrp-expr-%s/nw-ethtype-3\" id=\"3\"  placement=\"end\" value=%s />\n" % (ogroup_dn_name, condition_number, range_value[1]))
            	output.write("</pair>\n")

	    else :
		output.write("<pair key=\"%s/objgrp-expr-%s/nw-ethtype-2\">\n" % (ogroup_dn_name, condition_number))
            	output.write("<policyEthertype dn=\"%s/objgrp-expr-%s/nw-ethtype-2\" id=\"2\" value=%s />\n" % (ogroup_dn_name, condition_number, value))
            	output.write("</pair>\n")

	elif "service" in attr_name:
	    values = value.split()
	    if len(values) == 1 :
		any_port = "yes"
		port_num = 0
	    else :
		any_port = "no"
		port_num = values[1]
	    
	    output.write("<pair key=\"%s/objgrp-expr-%s/service\">\n" % (ogroup_dn_name, condition_number))
            output.write("<policyService dn=\"%s/objgrp-expr-%s/service\" id=\"2\" name=\"\" />\n" % (ogroup_dn_name, condition_number))
            output.write("</pair>\n")

	    output.write("<pair key=\"%s/objgrp-expr-%s/service/nw-protocol-2\">\n" % (ogroup_dn_name, condition_number))
            output.write("<policyProtocol dn=\"%s/objgrp-expr-%s/service/nw-protocol-2\" id=\"2\" placement=\"none\" name=\"\" value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, values[0]))
            output.write("</pair>\n")

	    if operator_type == "in-range"  or operator_type == "not-in-range":
		range_value = value.split()
		output.write("<pair key=\"%s/objgrp-expr-%s/service/nw-port-1\">\n" % (ogroup_dn_name, condition_number))
            	output.write("<policyNetworkPort anyPort=\"no\" dn=\"%s/objgrp-expr-%s/service/nw-port-1\" id=\"1\" placement=\"begin\" name=\"\" value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, values[1]))
            	output.write("</pair>\n")

		output.write("<pair key=\"%s/objgrp-expr-%s/service/nw-port-2\">\n" % (ogroup_dn_name, condition_number))
            	output.write("<policyNetworkPort anyPort=\"no\" dn=\"%s/objgrp-expr-%s/service/nw-port-2\" id=\"2\" placement=\"end\" name=\"\" value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, values[2]))
            	output.write("</pair>\n")

	    else :
	    	if values[0] == '6' or values[0] == '17' or values[0] == 'TCP' or values[0] == 'UDP' :
	    	    output.write("<pair key=\"%s/objgrp-expr-%s/service/nw-port-2\">\n" % (ogroup_dn_name, condition_number))
            	    output.write("<policyNetworkPort anyPort=\"%s\" dn=\"%s/objgrp-expr-%s/service/nw-port-2\" id=\"2\" placement=\"begin\" name=\"\" value=\"%s\"/>\n" % (any_port, ogroup_dn_name, condition_number, port_num))
            	    output.write("</pair>\n")

	elif "vm.cluster-name" in attr_name:
	    output.write("<pair key=\"%s/objgrp-expr-%s/compute-cluster-2\"> \n" % (ogroup_dn_name, condition_number))
    	    output.write("          <policyClusterName dn=\"%s/objgrp-expr-%s/compute-cluster-2\" id=\"2\"  value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, value))
	    output.write("</pair>")

	elif "vm.os-fullname" in attr_name:
	    output.write("<pair key=\"%s/objgrp-expr-%s/compute-osfname-2\"> \n" % (ogroup_dn_name, condition_number))
            output.write("           <policyGuestOSFullName dn=\"%s/objgrp-expr-%s/compute-osfname-2\" id=\"2\"  value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, value))
	    output.write("</pair>")

	elif "vm.host-name" in attr_name:
	    output.write("<pair key=\"%s/objgrp-expr-%s/compute-hypervisor-2\"> \n" % (ogroup_dn_name, condition_number))
            output.write("           <policyHypervisorName dn=\"%s/objgrp-expr-%s/compute-hypervisor-2\" id=\"2\"  value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, value))
	    output.write("</pair>")

	elif "vm.vapp-name" in attr_name:	
	    output.write("<pair key=\"%s/objgrp-expr-%s/compute-app-2\"> \n" % (ogroup_dn_name, condition_number))
            output.write("            <policyParentAppName dn=\"%s/objgrp-expr-%s/compute-app-2\" id=\"2\"  value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, value))
	    output.write("</pair>")

	elif "vm.pp-name" in attr_name:
            output.write("<pair key=\"%s/objgrp-expr-%s/compute-ppname-2\">\n" % (ogroup_dn_name, condition_number))
            output.write("    <policyPortProfileName dn=\"%s/objgrp-expr-%s/compute-ppname-2\"  id=\"2\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, value))
            output.write("</pair>\n")

	elif "vm.resource-pool" in attr_name:
            output.write("<pair key=\"%s/objgrp-expr-%s/compute-res-pool-2\">\n" % (ogroup_dn_name, condition_number))
            output.write("    <policyResourcePool dn=\"%s/objgrp-expr-%s/compute-res-pool-2\"  id=\"2\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, value))
            output.write("</pair>\n")

	elif "vm.os-hostname" in attr_name:
            output.write("<pair key=\"%s/objgrp-expr-%s/compute-hostname-2\">\n" % (ogroup_dn_name, condition_number))
            output.write("    <policyHostname dn=\"%s/objgrp-expr-%s/compute-hostname-2\" id=\"2\" value=\"%s\" />\n" % (ogroup_dn_name, condition_number, value))
            output.write("</pair>\n")

        elif "vm.name" in attr_name:
	    output.write("<pair key=\"%s/objgrp-expr-%s/compute-inst-2\">\n" % (ogroup_dn_name, condition_number))
            output.write("          <policyInstName dn=\"%s/objgrp-expr-%s/compute-inst-2\" id=\"2\" value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, value))
	    output.write("</pair>\n")
	
	elif "zone.name" in attr_name:
	    output.write("<pair key=\"%s/objgrp-expr-%s/zone-ref\">" % (ogroup_dn_name, condition_number))
            output.write("          <policyZoneNameRef dn=\"%s/objgrp-expr-%s/zone-ref\" value=\"%s\"/>\n" % (ogroup_dn_name, condition_number, value))
            output.write("</pair>")

    else:
        output.write("<pair key=\"%s/objgrp-expr-%s\">\n" % (ogroup_dn_name, condition_number))
        output.write("          <policyObjectGroupExpression dn=\"%s/objgrp-expr-%s\" id=\"%s\" status=\"deleted\"/>\n" % (ogroup_dn_name, condition_number, condition_number))
        output.write("</pair>\n")

def print_objgrp(set_mode, output, vdc, tenant, app, tier, ogroupname, descr, attr_name, conditions):
    if tier != "":
        obg_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + tier + "/objgrp-" + ogroupname
    elif app != "":
        obg_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/objgrp-" + ogroupname
    elif vdc != "":
        obg_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/objgrp-" + ogroupname
    elif tenant != "":
        obg_dn_name = "org-root/org-" + tenant + "/objgrp-" + ogroupname
    else :
        obg_dn_name = "org-root/objgrp-" + ogroupname

    if set_mode == "yes":
        output.write("<pair key=\"%s\">\n" % obg_dn_name)
        output.write("<policyObjectGroup descr=\"%s\" dn=\"%s\" name=\"%s\" />\n" % (descr, obg_dn_name, ogroupname))
        output.write("</pair>\n")

	for number1, operator_type1, value1 in zip(conditions[1],conditions[2],conditions[3]) :
	    print_objgrp_condition(set_mode, output, vdc, tenant, app, tier, ogroupname, number1, attr_name, operator_type1, value1)
    else:
        output.write("<pair key=\"%s\">\n" % obg_dn_name)
        output.write("<policyObjectGroup dn=\"%s\" name=\"%s\" status=\"deleted\"/>\n" % (obg_dn_name, ogroupname))
        output.write("</pair>\n")

def print_rule_condition(set_mode, output, vdc, tenant, app, tier, policyname, rule_name, condition_number, attr_name, operator_type, value):
	if tier != "":
		cond_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + \
app + "/org-" + tier + "/pol-" + policyname + "/rule-" + rule_name + \
"/rule-cond-" + condition_number
	elif app != "":
		cond_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + \
app + "/pol-" + policyname + "/rule-" + rule_name + "/rule-cond-" + \
condition_number
	elif vdc != "":
		cond_dn_name = "org-root/org-" + tenant + "/org-" + vdc + "/pol-" + \
policyname + "/rule-" + rule_name + "/rule-cond-" + condition_number
	elif tenant != "":
		cond_dn_name = "org-root/org-" + tenant + "/pol-" + policyname + \
"/rule-" + rule_name + "/rule-cond-" + condition_number
	else :
		cond_dn_name = "org-root/pol-" + policyname + "/rule-" + rule_name + \
"/rule-cond-" + condition_number

	source_destination = ""
	attr_expression = ""

	if set_mode == "yes":
		output.write("<pair key=\"%s\">" % cond_dn_name)
		output.write("        <policyRuleCondition dn=\"%s\" id=\"%s\"/> \n" % (cond_dn_name, condition_number))
		output.write("</pair>\n")

		if "src" in attr_name:
			source_destination = "source"
		if "dst" in attr_name:
			source_destination = "destination"
            
		if "custom" in attr_name:
			attr_expression = "policyNetworkCustomExpression"
			vnmc_operator = variables.vnmc_operator_equivalent[operator_type]

			output.write("<pair key=\"%s/nw-cust-expr2\">" % cond_dn_name)
			output.write("<%s dn=\"%s/nw-cust-expr2\" id=\"2\" opr=\"%s\"/>\n" % (attr_expression, cond_dn_name, vnmc_operator))
			output.write("</pair>\n")
            
			output.write("<pair key=\"%s/nw-cust-expr2/nw-attr-qual\">" % cond_dn_name)
			output.write("              <policyNwAttrQualifier attrEp=\"%s\" dn=\"%s/nw-cust-expr2/nw-attr-qual\"/>\n" % (source_destination, cond_dn_name))
			output.write("</pair>")
		else:
			attr_expression = "policyNetworkExpression"
			vnmc_operator = variables.vnmc_operator_equivalent[operator_type]

			output.write("<pair key=\"%s/nw-expr2\">" % cond_dn_name)
			output.write("<%s dn=\"%s/nw-expr2\" id=\"2\" opr=\"%s\"/>\n" % (attr_expression, cond_dn_name, vnmc_operator))
			output.write("</pair>\n")
            
		if "protocol" not in attr_name and "ethertype" not in attr_name and "service" not in attr_name and "custom" not in attr_name :
			output.write("<pair key=\"%s/nw-expr2/nw-attr-qual\">" % cond_dn_name)
			output.write("              <policyNwAttrQualifier attrEp=\"%s\" dn=\"%s/nw-expr2/nw-attr-qual\"/>\n" % (source_destination, cond_dn_name))
			output.write("</pair>")

		if "ip-address" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\">" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef attrClassName=\"policyIPAddress\" dn=\"%s/nw-expr2/objgrp-ref\" objgrpName=\"%s\"/>\n" % (cond_dn_name, value))
				output.write("</pair>")
			elif operator_type == "in-range" or operator_type == "not-in-range":
				range_value = value.split()
				output.write("<pair key=\"%s/nw-expr2/nw-ip-2\">" % cond_dn_name)
				output.write("                  <policyIPAddress dn=\"%s/nw-expr2/nw-ip-2\" id=\"2\" placement=\"begin\" value=\"%s\" />\n" % (cond_dn_name, range_value[0]))
				output.write("</pair>\n")
				output.write("<pair key=\"%s/nw-expr2/nw-ip-3\">" % cond_dn_name)
				output.write("                  <policyIPAddress dn=\"%s/nw-expr2/nw-ip-2\" id=\"3\" placement=\"end\" value=\"%s\" />\n" % (cond_dn_name, range_value[1]))
				output.write("</pair>\n")
			elif operator_type == "prefix":
				prefix_value = value.split()
		#logger_dbg.debug('\n\nprefix_value=',prefix_value[0],'value=',value)
				output.write("<pair key=\"%s/nw-expr2/nw-ip-subnet2\">" % cond_dn_name)
				output.write("                  <policyIPSubnet dn=\"%s/nw-expr2/nw-ip-subnet2\" id=\"2\" value=\"%s\" />\n" % (cond_dn_name, prefix_value[1]))
				output.write("</pair>\n")
				output.write("<pair key=\"%s/nw-expr2/nw-ip-2\">" % cond_dn_name)
				output.write("                  <policyIPAddress dn=\"%s/nw-expr2/nw-ip-2\" id=\"2\" value=\"%s\" />\n" % (cond_dn_name, prefix_value[0]))
				output.write("</pair>\n")
			else:
				output.write("<pair key=\"%s/nw-expr2/nw-ip-2\">" % cond_dn_name)
				output.write("                  <policyIPAddress id=\"2\" value=\"%s\" />\n" % value)
				output.write("</pair>\n")
 
		elif "protocol" in attr_name:      
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\">" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef dn=\"%s/nw-expr2/objgrp-ref\" objgrpName=\"%s\" attrClassName=\"policyProtocol\"/>\n" % ( cond_dn_name, value))
				output.write("</pair>\n")
			elif operator_type == "in-range" or operator_type == "not-in-range":
				range_value = value.split()
				output.write("<pair key=\"%s/nw-expr2/nw-protocol-2\">" % cond_dn_name)
				output.write("                  <policyProtocol dn=\"%s/nw-expr2/nw-protocol-2\" id=\"2\" placement=\"begin\" value=\"%s\" />\n" % (cond_dn_name, range_value[0]))
				output.write("</pair>\n")

				output.write("<pair key=\"%s/nw-expr2/nw-protocol-3\">" % cond_dn_name)
				output.write("                  <policyProtocol dn=\"%s/nw-expr2/nw-protocol-3\" id=\"3\" placement=\"end\" value=\"%s\" />\n" % (cond_dn_name, range_value[1]))
				output.write("</pair>\n")
			else:
				output.write("<pair key=\"%s/nw-expr2/nw-protocol-2\">" % cond_dn_name)
				output.write("          <policyProtocol dn=\"%s/nw-expr2/nw-protocol-2\" id=\"2\" value=\"%s\" />\n" % (cond_dn_name, value))
				output.write("</pair>\n")      
            
		elif "port" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\">" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef dn=\"%s/nw-expr2/objgrp-ref\" objgrpName=\"%s\" attrClassName=\"policyNetworkPort\"/>\n" % (cond_dn_name, value))
				output.write("</pair>\n")
			elif operator_type == "in-range" or operator_type == "not-in-range":
				range_value = value.split()
				output.write("<pair key=\"%s/nw-expr2/nw-port-2\">" % cond_dn_name)
				output.write("                  <policyNetworkPort dn=\"%s/nw-expr2/nw-port-2\" id=\"2\" placement=\"begin\" value=\"%s\" />\n" % (cond_dn_name, range_value[0]))
				output.write("</pair>\n")
				output.write("<pair key=\"%s/nw-expr2/nw-port-3\">" % cond_dn_name)
				output.write("                  <policyNetworkPort dn=\"%s/nw-expr2/nw-port-3\" id=\"3\" placement= \"end\" value=\"%s\" />\n" % (cond_dn_name, range_value[1]))
				output.write("</pair>\n")
			else:
				output.write("<pair key=\"%s/nw-expr2/nw-port-2\">" % cond_dn_name)
				output.write("                  <policyNetworkPort dn=\"%s/nw-expr2/nw-port-2\" id=\"2\" placement=\"0\" value=\"%s\" />\n" % (cond_dn_name, value))
				output.write("</pair>\n")

		elif "vm.cluster-name" in attr_name:
			attr_attribute = "policyClusterName"
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyClusterName\"/>\n" % value)
				output.write("</pair>")
			else:
				output.write("<pair key=\"%s/nw-expr2/compute-cluster-2\"> \n" % cond_dn_name)
				output.write("                  <policyClusterName dn=\"%s/nw-expr2/compute-cluster-2\" id=\"2\"  value=\"%s\"/>\n" % (cond_dn_name, value))
				output.write("</pair>")

		elif "vm.os-fullname" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyGuestOSFullName\"/>\n" % value)
				output.write("</pair>")
			else:
				output.write("<pair key=\"%s/nw-expr2/compute-osfname-2\"> \n" % cond_dn_name)
				output.write("           <policyGuestOSFullName dn=\"%s/nw-expr2/compute-osfname-2\" id=\"2\"  value=\"%s\"/>\n" % (cond_dn_name, value))
				output.write("</pair>")

		elif "vm.host-name" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyHypervisorName\"/>\n" % value)
				output.write("</pair>")
			else:
				output.write("<pair key=\"%s/nw-expr2/compute-hypervisor-2\"> \n" % cond_dn_name)
				output.write("           <policyHypervisorName dn=\"%s/nw-expr2/compute-hypervisor-2\" id=\"2\"  value=\"%s\"/>\n" % (cond_dn_name, value))
				output.write("</pair>")

		elif "vm.vapp-name" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyParentAppName\"/>\n" % value)
				output.write("</pair>")
			else:		
				output.write("<pair key=\"%s/nw-expr2/compute-app-2\"> \n" % cond_dn_name)
				output.write("            <policyParentAppName dn=\"%s/nw-expr2/compute-app-2\" id=\"2\"  value=\"%s\"/>\n" % (cond_dn_name, value))
				output.write("</pair>")

		elif "vm.pp-name" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyPortProfileName\"/>\n" % value)
				output.write("</pair>\n")
			else:
				output.write("<pair key=\"%s/nw-expr2/compute-ppname-2\">\n" % cond_dn_name)
				output.write("    <policyPortProfileName dn=\"%s/nw-expr2/compute-ppname-2\"  id=\"2\" value=\"%s\" />\n" % (cond_dn_name, value))
				output.write("</pair>\n")

		elif "vm.resource-pool" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyResourcePool\"/>\n" % value)
				output.write("</pair>\n")
			else:
				output.write("<pair key=\"%s/nw-expr2/compute-res-pool-2\">\n" % cond_dn_name)
				output.write("    <policyResourcePool dn=\"%s/nw-expr2/compute-res-pool-2\"  id=\"2\" value=\"%s\" />\n" % (cond_dn_name, value))
				output.write("</pair>\n")

		elif "vm.os-hostname" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyHostname\"/>\n" % value)
				output.write("</pair>\n")
			else:
				output.write("<pair key=\"%s/nw-expr2/compute-hostname-2\">\n" % cond_dn_name)
				output.write("    <policyHostname dn=\"%s/nw-expr2/compute-hostname-2\" id=\"2\" value=\"%s\" />\n" % (cond_dn_name, value))
				output.write("</pair>\n")

		elif "vm.name" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyInstName\"/>\n" % value)
				output.write("</pair>\n")
			else:
				output.write("<pair key=\"%s/nw-expr2/compute-inst-2\">\n" % cond_dn_name)
				output.write("          <policyInstName dn=\"%s/nw-expr2/compute-inst-2\" id=\"2\" value=\"%s\"/>\n" % (cond_dn_name, value))
				output.write("</pair>\n")

		elif "vm.custom" in attr_name:
			values = value.split()
			output.write("<pair key=\"%s/nw-cust-expr2/attr-ref\">\n" % cond_dn_name)
			output.write("          <policyAttributeDesignator attrName=\"%s\" dictionary=\"SecurityProfileCustomAttribute\" dn=\"%s/nw-cust-expr2/attr-ref\" />\n" % (values[0], cond_dn_name))
			output.write("</pair>\n")
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-cust-expr2/objgrp-ref\">\n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName= \
\"%s\"/>\n" % values[1])
				output.write("</pair>\n")
			else:
				output.write("<pair key=\"%s/nw-cust-expr2/attr-val2\">\n" % cond_dn_name)
				output.write("           <policyAttributeValue dn=\"%s/nw-cust-expr2/attr-val2\" value=\"%s\" id=\"2\" />\n" % (cond_dn_name, values[1]))
				output.write("</pair>\n")
     		
		elif "zone.name" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyZoneNameRef\"/>\n" % value)
				output.write("</pair>")
			else:
				output.write("<pair key=\"%s/nw-expr2/zone-ref\">" % cond_dn_name)
				output.write("           <policyZoneNameRef dn=\"%s/nw-expr2/zone-ref\" value=\"%s\"/>\n" % (cond_dn_name, value))
				output.write("</pair>")

		elif "ethertype" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyEthertype\"/>\n" % value)
				output.write("</pair>\n")
			elif operator_type == "in-range"  or operator_type == "not-in-range":
				range_value = value.split()
				output.write("<pair key=\"%s/nw-expr2/nw-ethtype-2\">\n" % cond_dn_name)
				output.write("<policyEthertype dn=\"%s/nw-expr2/nw-ethtype-2\" id=\"2\"   placement=\"begin\" value=%s />\n" % (cond_dn_name, range_value[0]))
				output.write("</pair>\n")
		
				output.write("<pair key=\"%s/nw-expr2/nw-ethtype-3\">\n" % cond_dn_name)
				output.write("<policyEthertype dn=\"%s/nw-expr2/nw-ethtype-3\" id=\"3\"  placement=\"end\" value=%s />\n" % (cond_dn_name, range_value[1]))
				output.write("</pair>\n")
			else :
				output.write("<pair key=\"%s/nw-expr2/nw-ethtype-2\">\n" % cond_dn_name)
				output.write("<policyEthertype dn=\"%s/nw-expr2/nw-ethtype-2\" id=\"2\" value=%s />\n" % (cond_dn_name, value))
				output.write("</pair>\n")
		elif "service" in attr_name:
			if operator_type == "member-of" or operator_type == "not-member-of":
				output.write("<pair key=\"%s/nw-expr2/objgrp-ref\"> \n" % cond_dn_name)
				output.write("           <policyObjectGroupNameRef objgrpName=\"%s\" attrClassName=\"policyService\"/>\n" % value)
				output.write("</pair>\n")
			else :
				values = value.split()
				if len(values) == 1 :
					any_port = "yes"
					port_num = 0
				else :
					any_port = "no"
					port_num = values[1]
				output.write("<pair key=\"%s/nw-expr2/service\">\n" % cond_dn_name)
				output.write("<policyService dn=\"%s/nw-expr2/service\" name=\"\" />\n" % cond_dn_name)
				output.write("</pair>\n")

				output.write("<pair key=\"%s/nw-expr2/service/nw-protocol-2\">\n" % cond_dn_name)
				output.write("<policyProtocol dn=\"%s/nw-expr2/service/nw-protocol-2\" id=\"2\" placement=\"none\" name=\"\" value=\"%s\"/>\n" % (cond_dn_name, values[0]))
				output.write("</pair>\n")
			if operator_type == "in-range"  or operator_type == "not-in-range":
				range_value = value.split()
				output.write("<pair key=\"%s/nw-expr2/service/nw-port-2\">\n" % cond_dn_name)
				output.write("<policyNetworkPort anyPort=\"no\" dn=\"%s/nw-expr2/service/nw-port-2\" id=\"2\" placement=\"begin\" name=\"\" value=\"%s\"/>\n" % (cond_dn_name, values[1]))
				output.write("</pair>\n")

				output.write("<pair key=\"%s/nw-expr2/service/nw-port-3\">\n" % cond_dn_name)
				output.write("<policyNetworkPort anyPort=\"no\" dn=\"%s/nw-expr2/service/nw-port-3\" id=\"3\" placement=\"end\" name=\"\" value=\"%s\"/>\n" % (cond_dn_name, values[2]))
				output.write("</pair>\n")

			else :
				values = value.split()
				if values[0] == '6' or values[0] == '17' or values[0] == 'TCP' or values[0] == 'UDP' :
					output.write("<pair key=\"%s/nw-expr2/service/nw-port-2\">\n" % cond_dn_name)
					output.write("<policyNetworkPort anyPort=\"%s\" dn=\"%s/nw-expr2/service/nw-port-2\" id=\"2\" placement=\"none\" name=\"\" value=\"%s\"/>\n" % (any_port, cond_dn_name, port_num))
					output.write("</pair>\n")
    
	else:
		output.write("<pair key=\"%s\">" % cond_dn_name)
		output.write("        <policyRuleCondition dn=\"%s\" id=\"%s\" status=\"deleted\"/> \n" % (cond_dn_name, condition_number))
		output.write("</pair>\n")

    
def print_rule(set_mode, output, vdc, tenant, app, tier, policyname, rulename, main_order, description, action_type, conditions,match_cri):
	if tier != "":
		dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + \
tier + "/pol-" + policyname
		action_dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + \
 "/org-" + tier  + "/pol-" + policyname + "/rule-" + rulename + "/rule-action-0"
	elif app != "":
		dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/pol-" + \
policyname
		action_dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + \
"/pol-" + policyname + "/rule-" + rulename + "/rule-action-0"
	elif vdc != "":
		dn = "org-root/org-" + tenant + "/org-" + vdc + "/pol-" + policyname
		action_dn = "org-root/org-" + tenant + "/org-" + vdc + "/pol-" + \
policyname + "/rule-" + rulename + "/rule-action-0"
	elif tenant != "":
		dn = "org-root/org-" + tenant + "/pol-" + policyname
		action_dn = "org-root/org-" + tenant + "/pol-" + policyname + "/rule-" + \
rulename + "/rule-action-0"
	else :
		dn = "org-root/pol-" + policyname
		action_dn = "org-root/pol-" + policyname + "/rule-" + rulename + \
"/rule-action-0"
	action_number = 1

	if set_mode == "yes":
		for number1, order1, attr_name1, operator_type1, value1 in zip(conditions[1],conditions[2],conditions[3],conditions[4],conditions[5]) :
			print_rule_condition("yes", output, vdc, tenant, app, tier, \
policyname, rulename, number1, attr_name1, operator_type1, value1)

		output.write("<pair key=\"%s\">\n" % action_dn)
		output.write("        <fwpolicyAction actionType=\"%s\" dn=\"%s\" /> \n" % \
(action_type, action_dn))
		output.write("</pair>\n")

		output.write("<pair key=\"%s/rule-%s\">\n" % (dn, rulename))
		output.write("  <policyRule descr=\"%s\" dn=\"%s/rule-%s\" name=\"%s\" matchCriteria=\"%s\" order=\"%s\" status=\"created\"> \n" % (description, dn, rulename,rulename, match_cri, main_order))
		output.write("  </policyRule> \n")
		output.write("</pair> \n")
	else:
		output.write("<pair key=\"%s/rule-%s\">\n" % (dn, rulename))
		output.write("  <policyRule dn=\"%s/rule-%s\" name=\"%s\" status=\"deleted\"> \n" % (dn, rulename, rulename))
		output.write("  </policyRule> \n")
		output.write("</pair> \n")

def print_policy(set_mode, output, vdc, tenant, app, tier, policyname, desc):
	if tier != "":
		pol_dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + \
"/org-" + tier + "/pol-" + policyname
	elif app != "":
		pol_dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + \
"/pol-" + policyname
	elif vdc != "":
		pol_dn = "org-root/org-" + tenant + "/org-" + vdc + "/pol-" + policyname
	elif tenant != "":
		pol_dn = "org-root/org-" + tenant + "/pol-" + policyname
	else :
		pol_dn = "org-root/pol-" + policyname

	if set_mode == "yes":
		output.write("<policyRuleBasedPolicy descr=\"%s\" dn=\"%s\" name=\"%s\"> \n" % (desc, pol_dn, policyname))
		output.write("</policyRuleBasedPolicy> \n")
	else:
		output.write("\n<pair key=\"%s\">\n" % pol_dn)
		output.write("<policyRuleBasedPolicy dn=\"%s\"  status=\"deleted\">\n" % pol_dn)
		output.write("</policyRuleBasedPolicy>\n")
		output.write("</pair>\n")

def print_policy_set(set_mode, output, vdc, tenant, app, tier, psname, desc):
    if tier != "":
	dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + tier + "/pset-" + psname
    elif app != "":
	dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/pset-" + psname
    elif vdc != "":
	dn = "org-root/org-" + tenant + "/org-" + vdc + "/pset-" + psname
    elif tenant != "":
	dn = "org-root/org-" + tenant + "/pset-" + psname
    else :
	dn = "org-root/pset-" + psname

    if set_mode == "yes":
	output.write("<policyPolicySet descr=\"%s\" dn=\"%s\" name=\"%s\"\n" % (desc,dn,psname))
	output.write("status=\"created\"/> \n")
        #output.write("\n<pair key=\"%s\">\n" % dn)
        #output.write("  <policyPolicySet dn=\"%s\"  >\n" % dn)
        #output.write("     <policyPolicyNameRef policyName=\"%s\"/>\n" % policyname)
        #output.write("  </policyPolicySet>\n")
        #output.write("</pair>\n")
    else:
        output.write("\n<pair key=\"%s\">\n" % dn)
        output.write("<policyPolicySet dn=\"%s\"  status=\"deleted\">\n" % dn)
        output.write("</policyPolicySet>\n")
        output.write("</pair>\n")

def print_policy_set_with_policies(set_mode, output, vdc, tenant, app, tier, psname, desc, policies_list):
	if tier != "":
		dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + tier + "/pset-" + psname
	elif app != "":
		dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/pset-" + psname
	elif vdc != "":
		dn = "org-root/org-" + tenant + "/org-" + vdc + "/pset-" + psname
	elif tenant != "":
		dn = "org-root/org-" + tenant + "/pset-" + psname
	else :
		dn = "org-root/pset-" + psname

	if set_mode == "yes":
		for policyinfo in policies_list :
			values = policyinfo.split()
			policyname = values[0]
			order = values[1]
			#logger_dbg.debug(values[0],values[1])
			if policyname != "default" and psname != "default" :
				output.write("\n<pair key=\"%s/polref-%s\">\n" % (dn,policyname))       
				output.write("     <policyPolicyNameRef \n policyName=\"%s\"/>\n" % policyname)
				output.write("dn=\"%s/polref-%s\"\n" % (dn,policyname))
				output.write("order=\"%s\"  status=\"created\"" % order)
				output.write("</pair>\n")
			else :
				pass
				#logger_dbg.debug("default policy is already attched to default policyset ")
	else:
		output.write("\n<pair key=\"%s\">\n" % dn)
		output.write("<policyPolicySet dn=\"%s\"  status=\"deleted\">\n" % dn)
		output.write("</policyPolicySet>\n")
		output.write("</pair>\n")

def print_security_profile_dictionary_modify(output, tenant, dictname, custm_attr_list):
    if tenant == "":
        dn = "org-root/attr-dict-custom-" + dictname
    else :
        dn = "org-root/org-" + tenant + "/attr-dict-custom-" + dictname

    for custm_attr in custm_attr_list :
	output.write("<pair key=\"%s/attr-vnsp-custom-%s\" >\n" %(dn, custm_attr))
    	output.write("<policyVnspCustomAttr dataType=\"string\" descr=\"\" dn=\"%s/attr-vnsp-custom-%s\" " %(dn, custm_attr))
    	output.write(" name=\"%s\" status=\"created\" />\n" % (custm_attr))
    	output.write("</pair>")

def print_security_profile_dictionary_empty(output, tenant, dictname):
    if tenant == "":
        dn = "org-root/attr-dict-custom-" + dictname
    else :
        dn = "org-root/org-" + tenant + "/attr-dict-custom-" + dictname

    output.write("\n<policyVnspCustomDictionary descr=\"\" \n")
    output.write(" dn=\"%s\"  name=\"%s\" status=\"created\" />\n" % (dn, dictname))

def print_security_profile(set_mode, output, vdc, tenant, sp_name, description, policysetname):
    if tenant == "":
        dn = "org-root/vnsp-" + sp_name
    else:
	if vdc == "":
            dn = "org-root/org-" + tenant + "/vnsp-" + sp_name
	else:
	    dn = "org-root/org-" + tenant + "/org-" + vdc + "/vnsp-" + sp_name

    if set_mode == "yes":
        output.write("\n<pair key=\"%s\">\n" % dn)
        output.write("  <policyVirtualNetworkServiceProfile descr=\"%s\" dn=\"%s\"  name=\"%s\" policySetNameRef=\"%s\">\n" % (description, dn, sp_name, policysetname))
        output.write("  </policyVirtualNetworkServiceProfile>\n")
        output.write("</pair>\n")
    else:
        output.write("\n<pair key=\"%s\">\n" % dn)
        output.write("  <policyVirtualNetworkServiceProfile dn=\"%s\"  status=\"deleted\">\n" % dn)
        output.write("  </policyVirtualNetworkServiceProfile>\n")
        output.write("</pair>\n")

def print_security_profile_modify(set_mode, output, vdc, tenant, app, tier, sp_name, description, policysetname, custm_attr_list):
    #if tenant == "":
    #    dn = "org-root/vnsp-" + sp_name
    #else:
	#if vdc == "":
         #   dn = "org-root/org-" + tenant + "/vnsp-" + sp_name
	#else:
	    #dn = "org-root/org-" + tenant + "/org-" + vdc + "/vnsp-" + sp_name
    if tier != "":
	dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/org-" + tier + "/vnsp-" + sp_name
    elif app != "":
	dn = "org-root/org-" + tenant + "/org-" + vdc + "/org-" + app + "/vnsp-" + sp_name
    elif vdc != "":
	dn = "org-root/org-" + tenant + "/org-" + vdc + "/vnsp-" + sp_name
    elif tenant != "":
	dn = "org-root/org-" + tenant + "/vnsp-" + sp_name
    else :
	dn = "org-root/vnsp-" + sp_name

    if set_mode == "yes":
        output.write("\n<pair key=\"%s\">\n" % dn)
        output.write("  <policyVirtualNetworkServiceProfile descr=\"%s\" dn=\"%s\"  name=\"%s\" policySetNameRef=\"%s\">\n" % (description, dn, sp_name, policysetname))
        output.write("  </policyVirtualNetworkServiceProfile>\n")
        output.write("</pair>\n")

	id_num = 2
	for custm_attr in custm_attr_list :
	    values = custm_attr.split()
	    attr_name = values[0]
	    value = values[1]
	    output.write("\n\n<pair key=\"%s/vnsp-avp-%s\" >\n" %(dn, id_num))
	    output.write("<policyVnspAVPair descr=\"\" dn=\"%s/vnsp-avp-%s\" \n" %(dn, id_num))
	    output.write("id=\"%s\" name=\"\" status=\"created\"/>" % id_num)
	    output.write("</pair>")

	    output.write("<pair key=\"%s/vnsp-avp-%s/attr-ref\" >\n" %(dn, id_num))
	    output.write("<policyAttributeDesignator attrName=\"%s\" dictionary=\"SecurityProfileCustomAttribute\" \n" %attr_name)
	    output.write("dn=\"%s/vnsp-avp-%s/attr-ref\" status=\"created\"/>" % (dn, id_num))
	    output.write("</pair>")

	    output.write("<pair key=\"%s/vnsp-avp-%s/attr-val2\" >\n" %(dn, id_num))
	    output.write("<policyAttributeValue dn=\"%s/vnsp-avp-%s/attr-val2\" \n" %(dn, id_num))
	    output.write("id=\"2\" status=\"created\" value=\"%s\" />" % value)
	    output.write("</pair>")
	    id_num += 1

    else:
        output.write("\n<pair key=\"%s\">\n" % dn)
        output.write("  <policyVirtualNetworkServiceProfile dn=\"%s\"  status=\"deleted\">\n" % dn)
        output.write("  </policyVirtualNetworkServiceProfile>\n")
        output.write("</pair>\n")

def get_cookie(vnmc_ip, username, password):
    request = "<aaaLogin\ninName=\"" + username + "\"\ninPassword=\"" + password + "\"/>"
    address = "https://" + vnmc_ip + "/xmlIM/policy-mgr/"

    i = datetime.datetime.now()
    if variables.time_flag == "False" :
	variables.start_time = i.minute
	variables.time_flag = "True"
	variables.stop_time = variables.start_time + 10
	if variables.stop_time > 60 :
	    variables.stop_time = variables.stop_time - 60
        response = requests.post(address, request, verify=False)
    	response = response.content.split(' ')
    	#cookie = ""
    	found = 0

    	for resp in response:
            temp = resp.split('=')
            if temp[0] == "outCookie":
            	variables.cookie = temp[1]
            	found = 1
            	break
    	if found == 0:
            print("Cookie not found. Config cannot be pushed to VNMC")
	    logger_err.error('Cookie not found. Config cannot be pushed to VNMC')
            exit()
    	else:
            return variables.cookie
    
    else :
	variables.current_time = i.minute
	if variables.current_time >= variables.stop_time :
	    logger_dbg.debug('time over')
	    #ch = raw_input("enter : ")
	    variables.time_flag = "False"
	return variables.cookie

def push_config(vnmc_ip, content, obj_mgr):
    address = "https://" + vnmc_ip + "/xmlIM/" + obj_mgr + "/"
    response = requests.post(address, content, verify=False)
    status_code = response.status_code
    logger_dbg.debug('status code of response is %s' %status_code)
    err = response.raise_for_status()
    logger_dbg.debug('raise_for_status is %s' %err) 
    logger_dbg.debug(response.text)
    response.close
    return response

#if __name__=="__main__":
#     cookie = get_cookie("172.20.75.54", "admin", "Sgate123")
#     output = StringIO.StringIO()
#     print_set_header(output, cookie)
     #print_security_profile("yes", output, "TM", "SP", "", "SP-PS")
     #print_policy("yes", output, "SP-PS", "SP-PS-P", "10")
#     conditions = [{"number":"2","order":"2","attr_name":"src.net.ip-address","operator_type":"eq","value":"10.10.10.1"},
#                   {"number":"3","order":"3","attr_name":"src.net.ip-address","operator_type":"neq","value":"10.10.10.2"},
#                   {"number":"4","order":"4","attr_name":"dst.net.ip-address","operator_type":"eq","value":"10.10.10.3"},
#                   {"number":"5","order":"5","attr_name":"dst.net.ip-address","operator_type":"neq","value":"10.10.10.4"}
#                  ]
#     print_rule("yes", output, "TM", "SP-PS-P", "Rule2", "0","", "permit", conditions)
#     print_set_footer(output)
#     response = push_config("172.20.75.54", output.getvalue(), "policy-mgr")
#     logger_dbg.debug(response)

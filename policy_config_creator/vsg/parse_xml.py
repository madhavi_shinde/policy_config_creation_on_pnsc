import os, sys
import json
import xml.etree.ElementTree as ET
from variables import *

tags = rule_cond_tags.copy()
def parse_attr_values(operator, netEx, attrTag):
    value1 = ""
    value2 = ""
    if operator == "range" or operator == "not-in-range":
        for attr in netEx.iter(attrTag):
            placement = attr.attrib[tags["placement"]]
            if placement == "begin":
                value1 = attr.attrib[tags["value"]]
            else:
                value2 = attr.attrib[tags["value"]]
    elif operator == "prefix":
        attr = netEx.find(attrTag)
        value1 = attr.attrib[tags["value"]]
        print value1
        attr = netEx.find(tags["subnet"])
        value2 = attr.attrib[tags["value"]]
        print value2
    else:
        attr = netEx.find(attrTag)
        value1 = attr.attrib[tags["value"]]
    return value1, value2

def parse_cond_table_xml2json(response):
    print "====================================="
    print response
    print "====================================="
    condition = dict()
    conditions = []
    root = ET.fromstring(response)
    for rule_cond in root.iter(tags["condition"]):
        condition.clear()
        cond_id = rule_cond.attrib[tags["cond-id"]]
        condition["id"] = int(cond_id)
        netEx = rule_cond.find(tags["net-expression"])
        if netEx != None:
            attr = ""
            value1 = ""
            value2 = ""
            value_str = ""
            operator = netEx.attrib[tags["operator"]]
            netDirct = netEx.find(tags["attr-qualifier"])
            if netDirct != None:
                direct = netDirct.attrib[tags["direction"]]
            else:
                direct = "None"
            port = netEx.find(tags["port"])
            if port != None:
                attr = attrs[tags["port"]]
                value1, value2 = parse_attr_values(operator,netEx,tags["port"])
            ip_addr = netEx.find(tags["ip-addr"])
            if ip_addr != None:
                attr = attrs[tags["ip-addr"]]
                value1, value2 = parse_attr_values(operator,netEx,tags["ip-addr"])
            protocol = netEx.find(tags["protocol"])
            if protocol != None:
                attr = attrs[tags["protocol"]]
                value1, value2 = parse_attr_values(operator,netEx,tags["protocol"])
            obj_grp = netEx.find(tags["obj-grp"])
            if obj_grp != None:
                attr = obj_grp.attrib[tags["obj-attr"]]
                attr = attrs[attr]
                value1 = obj_grp.attrib[tags["obj-grp-name"]]
            zone = netEx.find(tags["zone"])
            if zone != None:
                value1 = zone.attrib[tags["value"]]
                attr = "vZone"
            if operator == "range" or operator == "prefix" or operator == "not-in-range":
                value_str = "(%s" % value1 + "-" + "%s)" % value2
            else:
                value_str = value1
            condition["direction"] = direct
            condition["attr"] = attr
            condition["operator"] = operator
            condition["value"] = value_str
            print json.dumps(condition)
            conditions.append(json.loads(json.dumps(condition)))
    return conditions

def parse_rule_table_xml2json(response):
    tags = rule_tags.copy()
    rules = []
    rule = dict()
    root = ET.fromstring(response)
    for rule_item in root.iter(tags["rule"]):
        rule.clear()
        rule["id"] = int(rule_item.attrib[tags["rule-id"]])
        rule["name"] = rule_item.attrib[tags["name"]]
        rule["description"] = rule_item.attrib[tags["descr"]]
        act = rule_item.find(tags["actionTag"])
        # TODO: Add a logic to process multiple actions in a rule
        rule["action"] = act.attrib[tags["action"]]
        print json.dumps(rule)
        rules.append(json.loads(json.dumps(rule)))
    return rules

def parse_sptable_xml2json(response):
    sps = []
    sp = dict()
    root = ET.fromstring(response)
    for sp_item in root.iter("policyVirtualNetworkServiceProfile"):
        sp.clear()
        sp["id"] = int(sp_item.attrib["vnspId"])
        sp["name"] = sp_item.attrib["name"]
        sp["descr"] = sp_item.attrib["descr"]
        sps.append(json.loads(json.dumps(sp)))
    return sps

def parse_ogrouptable_xml2json(response):
    ogroups = []
    ogroup = dict()
    root = ET.fromstring(response)
    for ogroup_item in root.iter("policyObjectGroup"):
        ogroup.clear()
        ogroup["id"] = int(ogroup_item.attrib["intId"])
        ogroup["name"] = ogroup_item.attrib["name"]
        ogroup["descr"] = ogroup_item.attrib["descr"]
        ogroups.append(json.loads(json.dumps(ogroup)))
    return ogroups

def parse_zonetable_xml2json(response):
    zones = []
    zone = dict()
    root = ET.fromstring(response)
    for zone_item in root.iter("policyZone"):
        zone.clear()
        zone["id"] = int(zone_item.attrib["intId"])
        zone["name"] = zone_item.attrib["name"]
        zone["descr"] = zone_item.attrib["descr"]
        zones.append(json.loads(json.dumps(zone)))
    return zones

def parse_ogroup_cond_table_xml2json(response):
    condition = dict()
    conditions = []
    tags = obj_cond_tags.copy()
    root = ET.fromstring(response)
    for ogroup_cond in root.iter(tags["condition"]):
        condition.clear()
        ip_address = ""
        subnet = ""
        cond_id = ogroup_cond.attrib[tags["cond-id"]]
        operator = ogroup_cond.attrib[tags["operator"]]
        port = ogroup_cond.find(tags["port"])
        if port != None:
            attr = "net.port"
            value1, value2 = parse_attr_values(operator,ogroup_cond,tags["port"])
        ip_addr = ogroup_cond.find(tags["ip-addr"])
        if ip_addr != None:
            attr = "net.ip-address"
            value1, value2 = parse_attr_values(operator,ogroup_cond,tags["ip-addr"])
            if operator == "prefix":
                ip_address = value1
        protocol = ogroup_cond.find(tags["protocol"])
        if protocol != None:
            attr = "net.protocol"
            value1, value2 = parse_attr_values(operator,ogroup_cond,tags["protocol"])
        if operator == "prefix":
            subnet = ogroup_cond.find(tags["subnet"])
            if subnet != None:
                value1, value2 = parse_attr_values(operator,ogroup_cond,tags["subnet"])
                subnet = value1
                value_str = "(" + ip_address + "-" + subnet + ")"
        if operator == "range":
            value_str = "(" + value1 + "-" + value2 + ")"
        else:
            value_str = value1
        condition["id"] = int(cond_id)
        condition["attr_name"] = attr
        condition["operator"] = operator
        condition["value"] = value_str
        #print json.dumps(condition)
        conditions.append(json.loads(json.dumps(condition)))
    return conditions

def parse_zone_cond_table_xml2json(response):
    condition = dict()
    conditions = []
    tags = zone_cond_tags.copy()
    root = ET.fromstring(response)
    for zone_cond in root.iter(tags["condition"]):
        print "======================zone_cond=================="
        print zone_cond
        condition.clear()
        ip_address = ""
        subnet = ""
        cond_id = zone_cond.attrib[tags["cond-id"]]
        print "=============cond_id================"
        print cond_id
        netEx = zone_cond.find(tags["net-expression"])
        if netEx != None:
            operator = netEx.attrib[tags["operator"]]
        print "============operator==============="
        print operator
        ip_addr = netEx.find(tags["ip-addr"])
        print "=============ip-addr================"
        print ip_addr
        if ip_addr != None:
            attr = "net.ip-address"
            value1, value2 = parse_attr_values(operator,netEx,tags["ip-addr"])
            if operator == "prefix":
                ip_address = value1
        vm_name = netEx.find(tags["vm-name"])
        if vm_name != None:
            attr = "VM-Name"
            value1 = vm_name.attrib[tags["value"]]
        pp_name = netEx.find(tags["pp-name"])
        if pp_name != None:
            attr = "PP-Name"
            value1 = pp_name.attrib[tags["value"]]
#        if operator == "prefix":
#            subnet = ogroup_cond.find(tags["subnet"])
#            if subnet != None:
#                value1, value2 = parse_attr_values(operator,netEx,tags["subnet"])
#                subnet = value1
#                value_str = "(" + ip_address + "-" + subnet + ")"
        if operator == "range":
            value_str = "(" + value1 + "-" + value2 + ")"
        else:
            value_str = value1
        condition["id"] = int(cond_id)
        condition["attr_name"] = attr
        condition["operator"] = operator
        condition["value"] = value_str
        print "====================JSON JUMP OBJECT=============="
        print json.dumps(condition)
        conditions.append(json.loads(json.dumps(condition)))
    print "================Zone Conditions=================="
    print conditions
    return conditions

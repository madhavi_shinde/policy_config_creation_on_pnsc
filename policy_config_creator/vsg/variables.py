
vnmc_ip = "172.20.75.54"
vnmc_user = "admin"
vnmc_pass = "Sgate123"
time_flag = "False"
start_time = 0
stop_time = 0
current_time = 0
cookie = ""
test = ""

sec_pro_count_pass = 0
tenant_count_pass = 0
vdc_count_pass = 0
policyset_count_pass = 0
policy_count_pass = 0
rule_count_pass = 0
objgrp_count_pass = 0
zone_count_pass = 0
app_count_pass = 0
tier_count_pass = 0

sec_pro_count_total = 0
tenant_count_total = 0
vdc_count_total = 0
policyset_count_total = 0
policy_count_total = 0
rule_count_total = 0
objgrp_count_total = 0
zone_count_total = 0
app_count_total = 0
tier_count_total = 0

sec_pro_count_fail = 0
tenant_count_fail = 0
vdc_count_fail = 0
policyset_count_fail = 0
policy_count_fail = 0
rule_count_fail = 0
objgrp_count_fail = 0
zone_count_fail = 0
app_count_fail = 0
tier_count_fail = 0

attr_expression = ""
vnmc_operator_equivalent = {"eq": "eq",
                            "neq": "neq",
                            "in-range": "range",
                            "not-in-range": "not-in-range",
                            "member-of": "member",
                            "not-member-of": "not-member",
                            "prefix": "prefix",
                            "contains": "contains",
                            "gt": "gt",
                            "lt": "lt"}

attrs = {"policyNetworkPort": "net.port",
         "policyProtocol": "net.protocol",
         "policyIPAddress": "net.ip-address"}

rule_tags = {"rule": "policyRule",
             "rule-id": "order",
             "name": "name",
             "descr": "descr",
             "actionTag": "fwpolicyAction",
             "action-id": "id",
             "action": "actionType"}

rule_cond_tags = {"condition": "policyRuleCondition",
                  "cond-id": "id",
                  "net-expression": "policyNetworkExpression",
                  "operator": "opr",
                  "attr-qualifier": "policyNwAttrQualifier",
                  "direction": "attrEp",
                  "port": "policyNetworkPort",
                  "ip-addr": "policyIPAddress",
                  "protocol": "policyProtocol",
                  "subnet": "policyIPSubnet",
                  "obj-grp": "policyObjectGroupNameRef",
                  "obj-attr": "attrClassName",
                  "obj-grp-name": "objgrpName",
                  "zone" : "policyZoneNameRef",
                  "placement": "placement",
                  "value": "value"}

obj_cond_tags = {"condition": "policyObjectGroupExpression",
                  "cond-id": "id",
                  "operator": "opr",
                  "port": "policyNetworkPort",
                  "ip-addr": "policyIPAddress",
                  "protocol": "policyProtocol",
                  "subnet": "policyIPSubnet",
                  "placement": "placement",
                  "value": "value"}

zone_cond_tags = {"condition": "policyZoneCondition",
                  "cond-id": "id",
                  "net-expression": "policyZoneExpression",
                  "operator": "opr",
                  "ip-addr": "policyIPAddress",
                  "vm-name": "policyInstName",
                  "pp-name": "policyPortProfileName",
                  "placement": "placement",
                  "value": "value"}

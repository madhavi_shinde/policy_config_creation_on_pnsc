from main_files.vnmc import *

if __name__=="__main__":
    tenant_name = raw_input("Please enter name of tenant which you want to delete : ")
    response = tenant_delete(tenant_name)
    if "Error" in response :
        print "Tenant does not exist"
    else :
        print "Deleted tenant",tenant_name

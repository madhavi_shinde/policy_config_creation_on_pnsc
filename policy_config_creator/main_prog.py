# main program which will call different modules
from mainfiles.creating_diff_configs import *
from mainfiles.ConfBlk1 import *
from mainfiles.ConfBlk2 import *
from mainfiles.ConfBlk3 import *
from vsg.variables import *
from vsg.vnmc_xml import *
from mainfiles.vnmc import *
from mainfiles.objgrp_create import *
from mainfiles.zone_create import *
from mainfiles.clean_up import *
from mainfiles.logger import *

def main() :
	vnmc_ip = ""
	vnmc_user = ""
	vnmc_pass = ""
	#delete previous files
	clean()
	mode = raw_input('Please specify mode for execution (Debug/Execution) :')
	if mode == "Debug" :
    	#Seperating different configs
		seperate_show_running_config()
		seperate_SPs()
		seperate_object_groups()
		seperate_zones()
		seperate_rules()
		seperate_policies()
		logger_dbg.debug('done with creating different text files')
	
    	#This function creates tenants,security-profiles and policy-sets and maintains
    	#link between them
		ch = raw_input('enter anything to execute SPLink.py  : ')
		CB1 = ConfBlk1()
		CB1.tenant_sp_ps_link()

		#This function creates object-groups
		ch = raw_input('enter anything to execute obj_seperator.py  : ')
		OG1 = ObjGrp()        
		objgrp_zone_dict = OG1.obj_grp_creator()

		#This function creates zones
		ch = raw_input('enter anything to execute zone_create.py  : ')
		Z1 = Zone()        
		Z1.zone_seperator()
		if len(objgrp_zone_dict) >= 1 :
			OG1.objgrp_zone_creator(objgrp_zone_dict)

    	#this function creates policies and rules and maintains link between them
		ch = raw_input('enter anything to execute RuleLink.py  : ')
		CB2 = ConfBlk2()        
		CB2.policy_rule_link()
		logger_dbg.debug ("Done with creating policies and rules")

    	#this function creates link between policysets and policies
		ch = raw_input('enter anything to execute PS_Policy_Link.py  : ')
		CB3 = ConfBlk3() 
		CB3.policyset_policy_link()
		logger_dbg.debug("Done with attaching policysets to policies")
		
    	# check configs on pnsc
		print 'Done with all,check configs on pnsc\n'
		print 'Summary of configs : \n'
		display_count_of_configs()
	else :
		#Seperating different configs
		seperate_show_running_config()
		seperate_SPs()
		seperate_object_groups()
		seperate_zones()
		seperate_rules()
		seperate_policies()
		logger_dbg.debug('done with creating different text files')
	
    	#this function creates tenants,security-profiles and policy-sets and maintains
    	#link between them
		CB1 = ConfBlk1()
		CB1.tenant_sp_ps_link()
		logger_dbg.debug ("Done with creating tenants ,SP and policysets")

		#This function creates object-groups
		OG1 = ObjGrp()        
		objgrp_zone_dict = OG1.obj_grp_creator()

		#This function creates zones
		Z1 = Zone()        
		Z1.zone_seperator()
		if len(objgrp_zone_dict) >= 1 :
			OG1.objgrp_zone_creator(objgrp_zone_dict)

    	#this function creates policies and rules and maintains link between them
		CB2 = ConfBlk2()        
		CB2.policy_rule_link()
		logger_dbg.debug ("Done with creating policies and rules")

    	#this function creates link between policysets and policies
		CB3 = ConfBlk3() 
		CB3.policyset_policy_link()
		logger_dbg.debug("Done with attaching policysets to policies")
		
    	# check configs on pnsc
		print 'Done with all,check configs on pnsc'
		print 'Summary of configs : '
		display_count_of_configs()

if __name__=="__main__":
	logger_dbg.debug("Starting to push config to PNSC")
	main() 



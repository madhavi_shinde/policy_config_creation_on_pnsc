# README #
**# Steps to execute policy config creation on PNSC using VSG tech-support file #** 

**Introduction**: Config creator is a automation tool, which is designed for generating policy configuration on PNSC(Prime Network Services Controller). This tool accepts tech-support file as input and generates policy configuration on PNSC. We can obtain tech-support file from VSG(Virtual Security Gateway) by running command as “show tech-support”. 

**Steps to execute configuration generator for PNSC are as follows:**

1.Download policy config creation code from bit-bucket by running command as “git clone https://madhavi_shinde@bitbucket.org/madhavi_shinde/pnsc-configuration-through-script.git ”.

2.Entire code is in policy_config_creator folder.

3.Install python requests module by running command as “apt-get install python-requests”

4.If you want to change PNSC ip-address,username and password then open config file variables.py in vsg folder and change value of vnmc_ip, vnmc_user, vnmc_pass variable.

5.Run main_prog.py file as python main_prog.py .

6.It will ask for mode : [Debug or Execution] 
     Debug mode is for debug purpose,it takes pause after 1block creation and execution mode is for continuous execution of script.

7.After this, it will ask for tech-support filename. You can give filename from any location but you have to specify path along with filename. 
      For ex: /home/abc/tech-support.txt.

8.After providing this information, it will start pushing configuration on PNSC one after another  and displays message on terminal as particular configuration is created.

9.If any configuration fails because of any issue like already created or any parameter is missing then it will display message as “<error description>, skipping it.” Also it stores error information in error_log file.

10.It maintains debug_log file for debugging purpose.

11.While executing, script generates different text files according to different configs available and stores it in configfiles folder. We use these files for creating configs. 

12.Main_prog.py program internally calls different objects of some classes. We have written our main logic in those classes like for creating security-profile, tenant, vdc, application, tier and policy-set , we use object of ConfBlk1 class. For creating policy and rule, we use object of ConfBlk2 class. For creating object-group and zone, we use ObjGrp and Zone class respectively. For linking this hierarchy ,we use object of ConfBlk3 class and which links policy-set to respected policies. All these classes are stored in mainfiles folder.

13.After execution completes, it will display summary of configs on terminal in short. If you want to see summary in details then you can see it in summary.txt file of outputfiles folder.

14.After each successful configuration generation, configuration will display on PNSC.

15.Now if you want to delete all configurations of 1 tenant then you can run delete_config.py as python delete_config.py. It will ask for tenant name and respected tenant will be deleted. 